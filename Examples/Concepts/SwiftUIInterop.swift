func buildUI(_ state: State) -> Ish.View<State, Action> {
  hstack {
    text("\(state.count)")
    
    ishIncrement()
    fromSwiftUI(swiftUIDecrement)
  }
}

func ishIncrement() -> Ish.View<State, Action> {
  vstack {
    text("This Ish view will increment the count")
    button(action: .increment, label: text("+"))
  }
}

func swiftUIDecrement(
  sendAction: @escaping (Action) -> (),
  context: Context<State, Action>
) -> some SwiftUI.View {
  VStack {
    Text("This SwiftUI view will decrement the count")
    Button { sendAction(.decrement) } label: { Text("-") }
    
    ishHelloWorld().toSwiftUI(context)
  }
}

func ishHelloWorld() -> Ish.View<State, Action> {
  text("Hello world from Ish!")
}
