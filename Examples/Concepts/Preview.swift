func viewToPreview(state: State) -> View<State, Action> {
  text("Hello world!")
}

struct MyPreview: PreviewProvider {
  static var previews: some SwiftUIView {
    startLoop(initialState: State(), buildUI: viewToPreview)
  }
}
