func myXCTAssertTest() {
  startUpdateOnlyLoop(
    initialState: State(),
    update: update,
    actionsAndStates: [
      (.increment, { $0.count == 1 }),
      (.decrement, { $0.count == 0 }),
      (.increment, { $0.count == 1 })
    ]
  ) { XCTAssert($0, $1) }
}
