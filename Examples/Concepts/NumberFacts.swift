import Foundation
import Ish

struct State {
  var count = 0
  var alertText: String?
  var textField: String = "start"
}

enum Action {
  case factAlertDismissed
  case decrementButtonTapped
  case incrementButtonTapped
  case numberFactButtonTapped
  case numberFactResponse(String?)
  case textFieldChanged(String)
}

func update(state: State, action: Action) -> [Response<State, Action>] {
  switch action {
  case .textFieldChanged(_):
    return [.sideTask { print("yeetus") }]
  case .factAlertDismissed:
    return [.modify(\.alertText =~ nil)]
  
  case .decrementButtonTapped:
    return [.modify(\.count -~ 1)]
    
  case .incrementButtonTapped:
    return [.modify(\.count +~ 1)]
    
  case .numberFactButtonTapped:
    return [.task {
        let url = "http://numbersapi.com/\(state.count)/trivia"
        return .numberFactResponse(try? await String(
          decoding: URLSession.shared.data(from: URL(string:url)!).0,
          as: UTF8.self
        ))
    }]
    
  case .numberFactResponse(nil):
    return [.modify(\.alertText =~ "Couldn't get fact")]
    
  case let .numberFactResponse(str):
    return [.modify(\.alertText =~ str)]
  }
}

func buildUI(state: State) -> View<State, Action> {
  if let text = state.alertText {
    return alert(text, .factAlertDismissed)
  } else {
    return vstack([
      hstack([
        button(action: .decrementButtonTapped, label: text("-")),
        text("\(state.count)"),
        button(action: .incrementButtonTapped, label: text("+"))
      ])
    ])
  }
}

@main
struct NumberFactsApp: App {
  var body: some Scene {
    WindowGroup {
      startLoop(
        initialState: State(),
        buildUI: buildUI,
        update: update
      )
    }
  }
}
