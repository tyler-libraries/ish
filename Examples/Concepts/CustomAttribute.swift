extension Attribute {
  public class func disabled(_ bool: Bool) -> Self {
    Self { AnyView($0.disabled(bool)) }
  }
}
