import SwiftUI
import Ish

@main
struct WidgetIshApp: App {
    var body: some Scene {
        WindowGroup { start() }
    }
}

func start() -> some SwiftUI.View {
    Main.startLoop(Main.State(), initialAction: .initialize)
}
