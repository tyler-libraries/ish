import Ish

struct AddPopup: Feature {
    struct State {
        var name: String = ""
    }
    
    enum Action {
        case initialAction
        case setName(String)
        case delegate(Delegate)
    }
    
    enum Delegate {
        case save(String)
    }
    
    static func update(state: State, action: Action) -> Responses {
        switch action {
        case .initialAction:
            \.name .~ "initial"
        case .setName(let name):
            \.name .~ name
        case .delegate(_): []
        }
    }
}
