import Ish

extension AddPopup: FeatureView {
    static func buildUI2(state: State) -> ViewF {
        text("yeet?")
    }
    static func buildUI(state: State) -> View<Action> {
        vstack(attributes: [
            .width(200),
            .height(200),
            .background(.gray),
            .rounded(),
            .dimBackground()
        ]) {
            textField(
                placeholder: "Placeholder",
                value: state.name,
                onChange: { .setName($0) },
                attributes: [
                    .background(.white),
                    .width(150),
                    .height(35),
                    .padding(.horizontal, 10),
                    .rounded()
                ]
            )
            button(
                action: .delegate(.save(state.name)),
                label: text(state.name.count > 0 ? "Save" : "Dismiss", attributes: [
                    .padding(.all, 10),
                    .if(state.name.count > 0, .background(.green)),
                    .background(.yellow),
                    .foregroundColor(.white),
                    .rounded(),
                ]),
                attributes: [
                    .width(170),
                    .height(40),
                    .padding(.top, 20),
                ]
            )
        }
    }
}
