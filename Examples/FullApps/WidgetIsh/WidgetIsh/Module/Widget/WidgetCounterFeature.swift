import Ish

struct WidgetCounter: Feature {
    struct State: Equatable {
        var count: Int = 0
    }
    
    enum Action {
        case initialize
        case decrement
    }
    
    static func update(state: State, action: Action) -> Responses {
        switch action {
        case .initialize:
            \.count +~ 2
            task { .decrement }
        case .decrement:
            \.count -~ 1
        }
    }
}
