import Ish

extension WidgetCounter: FeatureView {
    static func buildUI(state: State) -> View<Action> {
        hstack {
            btn(.decrement, "-", .black)
            text("\(state.count)")
        }
    }
}
