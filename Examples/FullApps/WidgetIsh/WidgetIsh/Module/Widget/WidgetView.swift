import Ish

extension Widget: FeatureView {
    static func buildUI(state: State) -> View<Action> {
        return hstack {
            btn(.halve, "halve", .red, small: true)
            btn(.delegate(.tapped(state)), state.name, .green)
            btn(.double, "double", .blue, small: true)
            
            WidgetCounter.buildUI(state.counter, /Action.counter)
        }
    }
}
