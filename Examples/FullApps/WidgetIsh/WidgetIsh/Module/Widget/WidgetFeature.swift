import Ish

struct Widget: Feature {
    struct State: Equatable {
        var name: String
        var counter = WidgetCounter.State()
    }
    
    enum Action {
        case initialize
        case halve
        case double
        
        case counter(WidgetCounter.Action)
        case delegate(Delegate)
    }
    
    enum Delegate {
        case tapped(State)
    }
    
    static func update(state: State, action: Action) -> Responses {
        switch action {
        case .initialize:
            task {
                .counter(.initialize)
            }
        case .halve:
            \.name .~ String(state.name.dropLast(state.name.count / 2))
        case .double:
            \.name .~ (state.name + state.name)
        case .counter(_):
            WidgetCounter.update(state, action, \State.counter, /Action.counter)
            
        case .delegate(_): []
        }
    }
    
}
