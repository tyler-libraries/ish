import Ish

struct Main: Feature {
    struct State {
        var counter = 0
        var widgets: [Widget.State] = []
        var addPopup: AddPopup.State?
        var name: String = ""
    }
    
    enum Action {
        case initialize
        
        case increment
        case decrement
        case showAddPopup
        
        case widget(Int, Widget.Action)
        case addPopup(AddPopup.Action)
    }
    
    static func update(state: State, action: Action) -> Responses {
        switch action {
        case .initialize: addWidget(state, name: "Tap to Remove")
            
        case .increment: \.counter +~ 1
        case .decrement: \.counter -~ 1
            
        case .showAddPopup:
            \.addPopup .~ AddPopup.State()
            
        case .widget(_, .delegate(.tapped(let widget))):
            \.widgets -~ widget
        case .widget(_, _):
            Widget.update(state, action, \State.widgets, /Action.widget)
            
        case .addPopup(.delegate(.save(let name))):
            if name.count > 0 { addWidget(state, name: name) }
            \.addPopup .~ nil
        case .addPopup(_):
            AddPopup.update(state, action, \State.addPopup, /Action.addPopup)
        }
    }
    
    @arrayBuilder<Response<State, Action>>
    static func addWidget(_ state: State, name: String) -> Responses {
        \.widgets +~ Widget.State(name: name)
        task { .widget(state.widgets.count, .initialize) }
    }
}
