import Ish
import CasePaths

extension Main: FeatureView {
    static func buildUI(state: State) -> View<Action> {
        zstack {
            if state.counter < 5 {
                vstack {
                    Widget.buildUI(state.widgets, /Action.widget)
                    counterView(state.counter)
                    addWidgetButton(state.widgets.count)
                }
            } else {
                countAlert(state.counter)
            }
            AddPopup.buildUI(state.addPopup, /Action.addPopup)
        }
    }
    
    private static func counterView(_ count: Int) -> View<Action> {
        hstack {
            btn(.increment, "+", .blue)
            text("\(count)")
            btn(.decrement, "-", .red)
        }
    }
    
    private static func addWidgetButton(_ widgetCount: Int) -> View<Action> {
        button(
            action: .showAddPopup,
            label: text("Add Widget", attributes: [
                .width(200),
                .height(44)
            ]),
            attributes: [
                .foregroundColor(.white),
                .if(widgetCount > 5, .background(.red)),
                .if(widgetCount > 3, .background(.orange)),
                .background(.blue)
            ]
        )
    }
    
    private static func countAlert(_ count: Int) -> View<Action> {
        alert(
            title: "Counter too high: \(count)",
            message: "God forbids you from these heights",
            button: .custom(.blue, "Decrement", .decrement),
            cancelButton: .custom(.red, "Increment 😱", .increment),
            dimBackground: true
        )
    }
}
