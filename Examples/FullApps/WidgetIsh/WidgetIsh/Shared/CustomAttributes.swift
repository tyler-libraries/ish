import Ish

extension Attribute {
    static func rounded() -> Self {
        .attribute { $0.cornerRadius(15) }
    }
}
