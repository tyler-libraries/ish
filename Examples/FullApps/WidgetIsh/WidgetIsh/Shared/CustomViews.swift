import Ish
import SwiftUI

extension FeatureView {
    static func btn<A>(
        _ action: A,
        _ label: String,
        _ color: Color,
        small: Bool = false
    ) -> Ish.View<A> {
        button(action: action, label: text(label), attributes: [
            .if(small, .font(.system(size: 9))),
            .foregroundColor(color)
        ])
    }
   
}

