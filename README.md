## Ish - An Elmish framework

### Examples

In the `Examples` folder you'll find two kinds of examples, either full app examples (runnable iOS apps), or concepts. The concepts will all be in the context of the following code (as well as assume proper imports):

```
struct State {
 var count = 0
}

enum Action {
  case increment
  case decrement
}

func update(state: State, action: Action) -> [Response<State, Action>] {
  switch action {
  case .increment:
    .modify(\.count +~ 1)
  case .decrement:
    .modify(\.count -~ 1)
  }
}
```
