import SwiftCompilerPlugin
import SwiftSyntax
import SwiftSyntaxBuilder
import SwiftSyntaxMacros

public struct MakeLensesMacro: PeerMacro {
    public static func expansion(
      of node: AttributeSyntax,
      providingPeersOf declaration: some DeclSyntaxProtocol,
      in context: some MacroExpansionContext
    ) throws -> [DeclSyntax] {
        guard let structDecl = declaration.as(StructDeclSyntax.self) else {
          fatalError("@MakeLenses only works on structs")
        }
        
        let structName = structDecl.identifier
        var lensDeclarations: [DeclSyntax] = []
        func createSetDecls(_ name: TokenSyntax) -> String {
          structDecl
            .memberBlock.members
            .compactMap { $0.decl.as(VariableDeclSyntax.self) }
            .flatMap { $0.bindings }
            .map { binding in
              if let bName = binding.pattern.as(IdentifierPatternSyntax.self) {
                if name == bName.identifier {
                  return "\(name): a /* t */"
                } else {
                  return "\(bName.identifier): b.\(bName.identifier)"
                }
              }
              return "FAILED"
            }.joined(separator: ",")
        }
        for member in structDecl.memberBlock.members {
            
            guard let varDecl = member.decl.as(VariableDeclSyntax.self) else {
                continue
            }
            
            guard varDecl.bindingKeyword.text == "let" else {
                throw fatalError("must use immutable `let` bindings, found: \(varDecl.bindingKeyword)")
            }
            
            for binding in varDecl.bindings {
                if
                    let name = binding.pattern.as(IdentifierPatternSyntax.self),
                    let type = binding.typeAnnotation?.type {
                    // throw CustomError.message("\(type)")
                    // throw CustomError.message(varName.text)
                    
                    let setDecl = createSetDecls(name.identifier)
                    let lensDecl: DeclSyntax = """
              static let \(name.identifier): Lens<\(structName), \(type)> =
                .init(
                  get: { $0.\(name.identifier) },
                  set: { a,b in \(structName)(\(raw: setDecl)) }
                )
              """
                    // lensDeclarations.append(varName)
                    lensDeclarations.append(lensDecl)
                }
            }
        }
        return lensDeclarations
            
    }
}

public struct MakeLensesPeerNonStaticMacro: PeerMacro {
    public static func expansion(
      of node: AttributeSyntax,
      providingPeersOf declaration: some DeclSyntaxProtocol,
      in context: some MacroExpansionContext
    ) throws -> [DeclSyntax] {
        guard let structDecl = declaration.as(StructDeclSyntax.self) else {
          fatalError("@MakeLenses only works on structs")
        }
        
        let structName = structDecl.identifier
        var lensDeclarations: [DeclSyntax] = []
        func createSetDecls(_ name: TokenSyntax) -> String {
          structDecl
            .memberBlock.members
            .compactMap { $0.decl.as(VariableDeclSyntax.self) }
            .flatMap { $0.bindings }
            .map { binding in
              if let bName = binding.pattern.as(IdentifierPatternSyntax.self) {
                if name == bName.identifier {
                  return "\(name): a /* t */"
                } else {
                  return "\(bName.identifier): b.\(bName.identifier)"
                }
              }
              return "FAILED"
            }.joined(separator: ",")
        }
        for member in structDecl.memberBlock.members {
            
            guard let varDecl = member.decl.as(VariableDeclSyntax.self) else {
                continue
            }
            
            guard varDecl.bindingKeyword.text == "let" else {
                throw fatalError("must use immutable `let` bindings, found: \(varDecl.bindingKeyword)")
            }
            
            for binding in varDecl.bindings {
                if
                    let name = binding.pattern.as(IdentifierPatternSyntax.self),
                    let type = binding.typeAnnotation?.type {
                    // throw CustomError.message("\(type)")
                    // throw CustomError.message(varName.text)
                    
                    let setDecl = createSetDecls(name.identifier)
                    let lensDecl: DeclSyntax = """
              let \(name.identifier): Lens<\(structName), \(type)> =
                .init(
                  get: { $0.\(name.identifier) },
                  set: { a,b in \(structName)(\(raw: setDecl)) }
                )
              """
                    // lensDeclarations.append(varName)
                    lensDeclarations.append(lensDecl)
                }
            }
        }
        return lensDeclarations
            
    }
}

public struct MakeStaticLensesMacro: MemberMacro {
    public static func expansion(
      of node: AttributeSyntax,
      providingMembersOf declaration: some DeclGroupSyntax,
      in context: some MacroExpansionContext
    ) throws -> [DeclSyntax] {
        guard let structDecl = declaration.as(StructDeclSyntax.self) else {
          fatalError("@MakeLenses only works on structs")
        }
        
        let structName = structDecl.identifier
        var lensDeclarations: [DeclSyntax] = []
        func createSetDecls(_ name: TokenSyntax) -> String {
          structDecl
            .memberBlock.members
            .compactMap { $0.decl.as(VariableDeclSyntax.self) }
            .flatMap { $0.bindings }
            .map { binding in
              if let bName = binding.pattern.as(IdentifierPatternSyntax.self) {
                if name == bName.identifier {
                  return "\(name): a /* t */"
                } else {
                  return "\(bName.identifier): b.\(bName.identifier)"
                }
              }
              return "FAILED"
            }.joined(separator: ",")
        }
        for member in structDecl.memberBlock.members {
            
            guard let varDecl = member.decl.as(VariableDeclSyntax.self) else {
                continue
            }
            
            guard varDecl.bindingKeyword.text == "let" else {
                throw fatalError("must use immutable `let` bindings, found: \(varDecl.bindingKeyword)")
            }
            
            for binding in varDecl.bindings {
                if
                    let name = binding.pattern.as(IdentifierPatternSyntax.self),
                    let type = binding.typeAnnotation?.type {
                    // throw CustomError.message("\(type)")
                    // throw CustomError.message(varName.text)
                    
                    let setDecl = createSetDecls(name.identifier)
                    let lensDecl: DeclSyntax = """
              static var \(name.identifier):  Lens<\(structName), \(type)> {
                .init(
                  get: { $0.\(name.identifier) },
                  set: { a,b in \(structName)(\(raw: setDecl)) }
                )
              }
              """
                    // lensDeclarations.append(varName)
                    lensDeclarations.append(lensDecl)
                }
            }
        }
        return lensDeclarations
            
    }
}

@main
struct IshMacrosPlugin: CompilerPlugin {
    let providingMacros: [Macro.Type] = [
        MakeLensesMacro.self, MakeStaticLensesMacro.self, MakeLensesPeerNonStaticMacro.self
    ]
}
