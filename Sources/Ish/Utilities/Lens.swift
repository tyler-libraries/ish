import Foundation

infix operator +~: AdditionPrecedence
infix operator -~: AdditionPrecedence
infix operator *~: AdditionPrecedence
infix operator /~: AdditionPrecedence
infix operator %~: AdditionPrecedence
infix operator .~: AdditionPrecedence
infix operator ~: MultiplicationPrecedence

infix operator .^: BitwiseShiftPrecedence
postfix operator ^

public struct Lens<A, X> {
    let get: (A) -> X
    let set: (X, A) -> A
    
    public init(get: @escaping (A) -> X, set: @escaping (X, A) -> A) {
        self.get = get
        self.set = set
    }
}

public struct Prism<A, X> {
    let up: (X) -> A
    let down: (A) -> X?
    
    public init(up: @escaping (X) -> A, down: @escaping (A) -> X?) {
        self.up = up
        self.down = down
    }
}

public struct MultiLens<A, X> {
    let get: (A) -> [X]
    let set: ([X], A) -> A
    
    public init (get: @escaping (A) -> [X], set: @escaping ([X], A) -> A) {
        self.get = get
        self.set = set
    }
}

// TODO: implement %~?


public func over<A, X>(_ lhs: Lens<A, X>, _ fn: @escaping (X) -> X, a: A) -> A {
    a |> lhs %~ fn
}

public func .^<A, X>(_ from: A, lens: Lens<A, X>) -> X {
    lens.get(from)
}
public postfix func ^<A, X>(_ lhs: Lens<A, X>) -> ((A) -> X) {
    lhs.get
}

public postfix func ^<A, X>(_ lhs: Prism<A, X>) -> ((A) -> X?) {
    lhs.down
}

public func ix<Whole, Part>(
  _ from: Lens<Whole, [Part]>,
  _ i: Int
) -> Lens<Whole, Part> {
  Lens(
    get: { whole in from.get(whole)[i] },
    set: { part, whole in
      let items = from.get(whole).replaceAtIndex(i, part)
      return from.set(items, whole)
    }
  )
}

public func traverse<A, X>() -> (Lens<A, [X]>) -> MultiLens<A, X> {
    { from in .init(get: from.get, set: from.set) }
}

//public func traverse<A>() -> MultiLens<[A], A> {
//    .init(
//        get: identity,
//        set: { xs, a in a }
//    )
//}



public func _some<A>() -> Prism<A?, A> {
    .init(up: identity, down: identity)
}

public func _success<A, E>() -> Prism<Result<A, E>, A> {
    .init(
        up: { Result.success($0) },
        down: { (result: Result<A, E>) -> A? in
            result.toOption().toOptional()
        }
    )
}

public func _failure<A, E>() -> Prism<Result<A, E>, E> {
    .init(
        up: { Result.failure($0) },
        down: { (result: Result<A, E>) -> E? in
            switch(result) {
            case .success(_): nil
            case .failure(let e): e
            }
        }
    )
}

public func identity<A>(_ a: A) -> A { a }

func main() {
    @makeLenses
    struct User {
        let name: String
        let numbers: [Int]
    }
    
    let user = User(name: "potato", numbers: [1,2,3])
    
    
    let num: Int? = 3
    let _ = num |> _some() .~ 4
    let _ = user |> User.numbers ~ traverse +~ 1
    let _ = user |> (User.name %~ { $0.uppercased() } >>> User.numbers .~ [1])
    
}
