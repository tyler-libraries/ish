import Foundation

public func ~<A, X, Y>(
  _ from: Lens<A, X>,
  _ to: Lens<X, Y>
) -> Lens<A, Y> {
    .init(
        get: { to.get(from.get($0)) },
        set: { from.set(to.set($0, from.get($1)), $1) }
    )
}

public func ~<A, X, Y>(
  _ from: @escaping () -> Prism<A, X>,
  _ to: @escaping () -> Prism<X, Y>
) -> Prism<A, Y> {
    .init(
        up: { from().up(to().up($0)) },
        down: { from().down($0).flatMap { to().down($0) } }
    )
}

public func ~<A, X, Y>(
    _ from: MultiLens<A, X>,
    _ to: MultiLens<X, Y>
) -> MultiLens<A, Y> {
    .init(
        get: { a in from.get(a).flatMap(to.get) },
        set: { ys, a in from.set(from.get(a).map { to.set(ys, $0) }, a) }
    )
}

//public func ~<A, X, Y>(
//    _ from: Lens<A, X>,
//    _ to: Prism<X, Y>
//) -> Prism<A, Y> {
//    .init(
//        up: { y in from.set(} ,
//        down: { a in }
//    )
//}
//public func ~<A, X, Y>(
//    _ lhs: Lens<A, X>,
//    _ rhs: @escaping () -> Prism<X, Y>
//) -> MultiLens<A, Y> {
//    .init(
//        get: { a in lhs.get(a) |> { rhs().down($0) } },
//        set: { ys, a in
//            let xs = ys.map(rhs().up)
//            
//            ys.map(rhs().up).map { lhs.set($0, a) }
//        }
////        up: { y in lhs.set(rhs().up(y), y) },
////        down: { a in lhs.get(a).flatMap(rhs().down) }
//    )
//}

public func ~<A, X>(
    _ from: Lens<A, [X]>,
    _ to: @escaping () -> ((Lens<A, [X]>) -> MultiLens<A, X>)
) -> MultiLens<A, X> {
    .init(
        get: { from.get($0) },
        // get: { to()(from).get($0) },
        set: { xs, a in
            from.set(xs, a)
        }
    )
}

public func ~<A, X, Y>(
    _ from: Lens<A, X>,
    _ to: @escaping () -> MultiLens<X, Y>
) -> MultiLens<A, Y> {
    .init(
        get: { a in to().get(from.get(a)) },
        set: { ys, a in
            let x = to().set(ys, from.get(a))
            return from.set(x, a)
        }
    )
}





//public func ~<A, X, Y>(
//    _ from: Lens<A, X>,
//    _ to: () -> Prism<X, Y>
//) -> MultiLens<A, Y> {
//    .init(
//        get: { a in from.get(a) |> { to().down($0).toOption().toOptional() } },
//        set: { ys, a in from.set(to().up(ys), a) }
//    )
//}
