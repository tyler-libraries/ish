import Foundation

public func .~<A, X>(_ lhs: Lens<A, X>, _ value: X) -> ((A) -> A) {
    { a in lhs.set(value, a) }
}

public func .~<A, X>(_ lhs: Prism<A, X>, _ value: X) -> ((A) -> A) {
    { a in lhs.down(a).map { _ in lhs.up(value) } ?? a }
}

public func .~<A, X>(_ lhs: MultiLens<A, X>, _ value: X) -> ((A) -> A) {
    { a in lhs.set(lhs.get(a).map { _ in value }, a) }
}


public extension Lens {
    func set(_ x: X) -> ((A) -> A) {
        self .~ x
    }
}

public extension Prism {
    func set(_ x: X) -> ((A) -> A) {
        self .~ x
    }
}

public extension MultiLens {
    func set(_ x: X) -> ((A) -> A) {
        self .~ x
    }
}
