import Foundation

public func %~<A, X>(_ lhs: Lens<A, X>, _ transform: @escaping (X) -> X) -> ((A) -> A) {
    { a in lhs.set(transform(lhs.get(a)), a) }
}

public func %~<A, X>(_ lhs: Prism<A, X>, _ transform: @escaping (X) -> X) -> ((A) -> A) {
    { a in lhs.down(a).map { x in lhs.up(transform(x)) } ?? a }
}

public func %~<A, X>(_ lhs: MultiLens<A, X>, _ transform: @escaping (X) -> X) -> ((A) -> A) {
    { a in lhs.set(lhs.get(a).map(transform), a) }
}

public extension Lens {
    func modify(_ transform: @escaping (X) -> X) -> ((A) -> A) {
        self %~ transform
    }
    
    func over(_ transform: @escaping (X) -> X) -> ((A) -> A) {
        modify(transform)
    }
}

public extension Prism {
    func modify(_ transform: @escaping (X) -> X) -> ((A) -> A) {
        self %~ transform
    }
    
    func over(_ transform: @escaping (X) -> X) -> ((A) -> A) {
        modify(transform)
    }
}

public extension MultiLens {
    func modify(_ transform: @escaping (X) -> X) -> ((A) -> A) {
        self %~ transform
    }
    
    func over(_ transform: @escaping (X) -> X) -> ((A) -> A) {
        modify(transform)
    }
}
