import Foundation

public func +~<A, X: Numeric>(_ lhs: Lens<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 + rhs }
}

public func -~<A, X: Numeric>(_ lhs: Lens<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 - rhs }
}

public func *~<A, X: Numeric>(_ lhs: Lens<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 * rhs }
}

public func /~<A, X: Numeric>(_ lhs: Lens<A, X>, _ rhs: X) -> ((A) -> A) where X: FloatingPoint {
    lhs %~ { $0 / rhs }
}

public func +~<A, X: Numeric>(_ lhs: Prism<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 + rhs }
}

public func -~<A, X: Numeric>(_ lhs: Prism<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 - rhs }
}

public func *~<A, X: Numeric>(_ lhs: Prism<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 * rhs }
}

public func /~<A, X: Numeric>(_ lhs: Prism<A, X>, _ rhs: X) -> ((A) -> A) where X: FloatingPoint {
    lhs %~ { $0 / rhs }
}

public func +~<A, X: Numeric>(_ lhs: MultiLens<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 + rhs }
}

public func -~<A, X: Numeric>(_ lhs: MultiLens<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 - rhs }
}

public func *~<A, X: Numeric>(_ lhs: MultiLens<A, X>, _ rhs: X) -> ((A) -> A) {
    lhs %~ { $0 * rhs }
}

public func /~<A, X: Numeric>(_ lhs: MultiLens<A, X>, _ rhs: X) -> ((A) -> A) where X: FloatingPoint {
    lhs %~ { $0 / rhs }
}
