import Foundation

public struct Reader<E, A> {
    public let runReader: (E) -> A
    
    public init(_ runReader: @escaping (E) -> A) {
        self.runReader = runReader
    }
    
    public func local(_ fn: @escaping (E) -> E) -> Reader<E, A> {
        Reader { runReader(fn($0)) }
    }
    
    public func map<B>(_ f: @escaping (A) -> B) -> Reader<E, B> {
        return .init { f(self.runReader($0)) }
    }
    
    public func ap<B>(_ f: Reader<E, (A) -> B>) -> Reader<E, B> {
        .init { r in
            f.runReader(r)(self.runReader(r))
        }
    }
    
    public func flatMap<B>(_ f: @escaping (A) -> Reader<E, B>) -> Reader<E, B> {
        .init { r in
            f(self.runReader(r)).runReader(r)
        }
    }
}

public struct AsyncReader<E, A> {
    public let runReader: (E) async -> A
    
    public init(_ runReader: @escaping (E) async -> A) {
        self.runReader = runReader
    }
    
    public func local(_ fn: @escaping (E) async -> E) async -> AsyncReader<E, A> {
        AsyncReader { await runReader(fn($0)) }
    }
    
    public func map<B>(_ f: @escaping (A) async -> B) async -> AsyncReader<E, B> {
        .init { await f(self.runReader($0)) }
    }
    
    public func ap<B>(_ f: AsyncReader<E, (A) async -> B>) async -> AsyncReader<E, B> {
        .init { r in
            await f.runReader(r)(self.runReader(r))
        }
    }
    
    public func flatMap<B>(_ f: @escaping (A) async -> AsyncReader<E, B>) async -> AsyncReader<E, B> {
        .init { r in
            await f(self.runReader(r)).runReader(r)
        }
    }
}

public struct ThrowingReader<E, A> {
    public let runReader: (E) throws -> A
    
    public init(_ runReader: @escaping (E) throws -> A) {
        self.runReader = runReader
    }
    
    public func local(_ fn: @escaping (E) throws -> E) rethrows -> ThrowingReader<E, A> {
        ThrowingReader { try runReader(fn($0)) }
    }
    
    public func map<B>(_ f: @escaping (A) throws -> B) rethrows -> ThrowingReader<E, B> {
        .init { try f(self.runReader($0)) }
    }
    
    public func ap<B>(_ f: Reader<E, (A) throws -> B>) -> ThrowingReader<E, B> {
        .init { r in
            try f.runReader(r)(self.runReader(r))
        }
    }
    
    public func flatMap<B>(_ f: @escaping (A) throws -> ThrowingReader<E, B>) rethrows -> ThrowingReader<E, B> {
        .init { r in
            try f(self.runReader(r)).runReader(r)
        }
    }
}

public struct AsyncThrowingReader<E, A> {
    public let runReader: (E) async throws -> A
    
    public init(_ runReader: @escaping (E) async throws -> A) {
        self.runReader = runReader
    }
    
    public func local(_ fn: @escaping (E) async throws -> E) async throws -> AsyncThrowingReader<E, A> {
        AsyncThrowingReader { try await runReader(fn($0)) }
    }
    
    public func map<B>(_ f: @escaping (A) async throws -> B) async throws -> AsyncThrowingReader<E, B> {
        return .init { try await f(self.runReader($0)) }
    }
    
    public func ap<B>(_ f: AsyncThrowingReader<E, (A) async throws -> B>) -> AsyncThrowingReader<E, B> {
        .init { r in
            try await f.runReader(r)(self.runReader(r))
        }
    }
    
    public func flatMap<B>(_ f: @escaping (A) async throws -> AsyncThrowingReader<E, B>) -> AsyncThrowingReader<E, B> {
        .init { r in
            try await f(self.runReader(r)).runReader(r)
        }
    }
}
//    public func flatMapWithEnv<B>(_ f: @escaping (E, A) -> Reader<E, B>) -> Reader<E, B> {
//        self.flatMap { a in
//            Reader<E, (E, A)> { env in
//                (ask().runReader(env), a)
//            }
//        }.flatMap(f)
//    }
// }

public func ask<E>() -> Reader<E, E> {
    Reader<E, E> { $0 }
}

public func pure<E, A>(_ a: A) -> Reader<E, A> {
    .init { _ in a }
}


typealias SQLDatabase = String

protocol HasName {
    static func getTypeName() -> String
}
struct Book: HasName {
    let name: String
    let date: Date
    
    static func getTypeName() -> String { "book" }
}

struct User: HasName {
    let name: String
    let favoriteBookNames: [String]
    
    static func getTypeName() -> String { "user" }
}

func fn<A: HasName>() -> (String, A?) {
    (A.getTypeName(), nil)
}

func getUser(_ uid: Int) async throws -> AsyncThrowingReader<SQLDatabase, User> {
    fatalError()
}
func getBook(_ name: String) async throws -> AsyncThrowingReader<SQLDatabase, Book> {
    fatalError()
}
func getOldestFavoriteBookForUserId(_ uid: Int) async throws -> AsyncThrowingReader<SQLDatabase, Book?> {
    try await getUser(uid)
        .ap(AsyncThrowingReader { db in
            { user in
                var result: Book? = nil
                for name in user.favoriteBookNames {
                    let book = try await getBook(name).runReader(db)
                    if let r = result, book.date < r.date {
                        result = book
                    }
                }
                return result
                // user.favoriteBookNames.map { getBook($0).runReader(env) }.sorted { $0.date < $1.date }.first
            }
        })
    // MAYBE USE
//    let userReader = try await getUser(uid)
//    userReader
//        .flatMap { user in
//            Reader<SQLDatabase, (SQLDatabase, User)> { env in
//                (userReader.ask().runReader(env), user)
//            }
//        }
//        .flatMap { (env: SQLDatabase, user: User) in
//            let books: [Book] = user.favoriteBookNames.map { getBook($0).runReader(env) }
//            
//        }
    
    
// DON'T USE
//    getUser(uid).flatMap { user in
//        
//        let books: [Book] = user.favoriteBookNames.map { getBook($0) }
//    }
}
//            .reduce(Reader<SQLDatabase, Book?> { _ in nil }) { acc, e in
//                acc.flatMap { aee in e }
//            }
