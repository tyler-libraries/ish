import Foundation

infix operator |> : LogicalConjunctionPrecedence
infix operator <| : LogicalConjunctionPrecedence
infix operator >>> : LogicalConjunctionPrecedence
infix operator <<< : LogicalConjunctionPrecedence

// MARK: Reverse function application
public func |><A, B>(a: A, fn: (A) -> B) -> B {
    fn(a)
}
public func |><A, B>(a: A, fn: (A) async -> B) async -> B {
    await fn(a)
}
public func |><A, B>(a: A, fn: (A) async throws -> B) async throws -> B {
    try await fn(a)
}


// MARK: Forward function application
public func <|<A, B>(fn: (A) -> B, a: A) -> B {
    fn(a)
}
public func <|<A, B>(fn: (A) async -> B, a: A) async -> B {
    await fn(a)
}
public func <|<A, B>(fn: (A) async throws -> B, a: A) async throws -> B {
    try await fn(a)
}


// MARK: Reverse function composition
public func >>><A, B, C>(f: @escaping (A) -> B, g: @escaping (B) -> C) -> (A) -> C {
    { $0 |> f |> g }
}

public func >>><A, B, C>(f: @escaping (A) async -> B, g: @escaping (B) async -> C) -> (A) async -> C {
    { await $0 |> f |> g }
}

public func >>><A, B, C>(f: @escaping (A) async throws -> B, g: @escaping (B) async throws -> C) -> (A) async throws -> C {
     { try await $0 |> f |> g }
}

