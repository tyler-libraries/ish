// TODO: note: had `lastItemAction` case here, half-baked, don't know why.
// I think it had something to do with initialAction being processed too much
public enum InternalResponse<S, A> {
    case modify((S) -> S)
    case task(fn: () async -> A)
    case throwableTask(onThrow: (Error) -> A, throwableFn: () async throws -> A)
    case sideTask(() async -> ())
    case throwableSideTask(onThrow: (Error) -> A, throwableFn: () async throws -> ())
    case noOp
}

public typealias Response<S, A> = Reader<(S, A), InternalResponse<S, A>>

public func noOp<S, A>() -> Response<S, A> {
    .noOp()
}

public func modify<S, A>(_ transform: @escaping (S) -> S) -> Response<S, A> {
    .modify(transform)
}

public extension Reader {
    static func noOp<T, R>() -> Response<T, R> {
        Reader<(T, R), InternalResponse<T, R>> { _ in InternalResponse.noOp }
    }
    
    static func modify<T, R>(_ transform: @escaping (T) -> T) -> Response<T, R> {
        Reader<(T, R), InternalResponse<T, R>> { _ in InternalResponse.modify(transform) }
    }
}
