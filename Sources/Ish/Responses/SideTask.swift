/// Simple variants: closure passed optionally takes a `State` (all variants have at least these 2 variations)
 
public func sideTask<S, A>(_ fn: @escaping () async -> ()) -> Response<S, A> {
    sideTask { _ in await fn() }
}

public func sideTask<S, A>(_ fn: @escaping (S) async -> ()) -> Response<S, A> {
    Reader { (state, _) in InternalResponse.sideTask({ await fn(state) }) }
}

/// onThrow variants: `fn` can now throw, and `onThrow` says what action to fire when that happens
 
public func sideTask<S, A>(
    onThrow: @escaping (Error) -> A,
    _ fn: @escaping () async throws -> ()
) -> Response<S, A> {
    sideTask(onThrow: onThrow) { _ in try await fn() }
}

public func sideTask<S, A>(
    onThrow: @escaping (Error) -> A,
    _ fn: @escaping (S) async throws -> ()
) -> Response<S, A> {
    Reader { (state, _) in
        InternalResponse.throwableSideTask(
            onThrow: onThrow,
            throwableFn: { try await fn(state) }
        )
    }
}

/// forEach variants: runs a task for each `SS`, an extra variant exists to also take the index of the `SS`

public func sideTask<S, A, SS>(
    forEach: [SS],
    _ fn: @escaping () async -> ()
) -> [Response<S, A>] {
    sideTask(forEach: forEach) { _, _ in await fn() }
}

public func sideTask<S, A, SS>(
    forEach: [SS],
    _ fn: @escaping (S) async -> ()
) -> [Response<S, A>] {
    sideTask(forEach: forEach) { _, s in await fn(s) }
}

public func sideTask<S, A, SS>(
    forEach: [SS],
    _ fn: @escaping (Int, S) async -> ()
) -> [Response<S, A>] {
    forEach.indices.map { i in sideTask { await fn(i, $0) } }
}

/// onThrow + forEach variants
 
public func sideTask<S, A, SS>(
    onThrow: @escaping (Error) -> A,
    forEach: [SS],
    _ fn: @escaping () async throws -> ()
) -> [Response<S, A>] {
    sideTask(onThrow: onThrow, forEach: forEach) { _, _ in try await fn() }
}

public func sideTask<S, A, SS>(
    onThrow: @escaping (Error) -> A,
    forEach: [SS],
    _ fn: @escaping (S) async throws -> ()
) -> [Response<S, A>] {
    sideTask(onThrow: onThrow, forEach: forEach) { _, s in try await fn(s) }
}

public func sideTask<S, A, SS>(
    onThrow: @escaping (Error) -> A,
    forEach: [SS],
    _ fn: @escaping (Int, S) async throws -> ()
) -> [Response<S, A>] {
    forEach.indices.map { i in sideTask(onThrow: onThrow) { try await fn(i, $0) } }
}
