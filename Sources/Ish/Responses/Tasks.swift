/// Simple variants: closure passed optionally takes a `State` (all variants have at least these 2 variations)
 
public func task<S, A>(_ fn: @escaping () async -> A) -> Response<S, A> {
    Response<S, A>.task(fn)
}

public func task<S, A>(_ fn: @escaping (S) async -> A) -> Response<S, A> {
    Response<S, A>.task(fn)
}

/// onThrow variants: `fn` can now throw, and `onThrow` says what action to fire when that happens
 
public func task<S, A>(
    onThrow: @escaping (Error) -> A,
    _ fn: @escaping () async throws -> A
) -> Response<S, A> {
    Response<S, A>.task(onThrow: onThrow, fn)
}

public func task<S, A>(
    onThrow: @escaping (Error) -> A,
    _ fn: @escaping (S) async throws -> A
) -> Response<S, A> {
    Response<S, A>.task(onThrow: onThrow, fn)
}

/// forEach variants: runs a task for each `SS`, an extra variant exists to also take the index of the `SS`

public func task<S, A, SS>(
    forEach: [SS],
    _ fn: @escaping () async -> A
) -> [Response<S, A>] {
    Response<S, A>.task(forEach: forEach, fn)
}

public func task<S, A, SS>(
    forEach: [SS],
    _ fn: @escaping (S) async -> A
) -> [Response<S, A>] {
    Response<S, A>.task(forEach: forEach, fn)
}

public func task<S, A, SS>(
    forEach: [SS],
    _ fn: @escaping (Int, S) async -> A
) -> [Response<S, A>] {
    Response<S, A>.task(forEach: forEach, fn)
}

/// onThrow + forEach variants
 
public func task<S, A, SS>(
    onThrow: @escaping (Error) -> A,
    forEach: [SS],
    _ fn: @escaping () async throws -> A
) -> [Response<S, A>] {
    Response<S, A>.task(onThrow: onThrow, forEach: forEach, fn)
}

public func task<S, A, SS>(
    onThrow: @escaping (Error) -> A,
    forEach: [SS],
    _ fn: @escaping (S) async throws -> A
) -> [Response<S, A>] {
    Response<S, A>.task(onThrow: onThrow, forEach: forEach, fn)
}

public func task<S, A, SS>(
    onThrow: @escaping (Error) -> A,
    forEach: [SS],
    _ fn: @escaping (Int, S) async throws -> A
) -> [Response<S, A>] {
    Response<S, A>.task(onThrow: onThrow, forEach: forEach, fn)
}

public extension Reader {
    /// Simple variants: closure passed optionally takes a `State` (all variants have at least these 2 variations)
     
    static func task<S, A>(_ fn: @escaping () async -> A) -> Response<S, A> {
        task { _ in await fn() }
    }

    static func task<S, A>(_ fn: @escaping (S) async -> A) -> Response<S, A> {
        Reader<(S,A), InternalResponse<S, A>> { (state, _) in InternalResponse.task(fn: { await fn(state) }) }
    }

    /// onThrow variants: `fn` can now throw, and `onThrow` says what action to fire when that happens
     
    static func task<S, A>(
        onThrow: @escaping (Error) -> A,
        _ fn: @escaping () async throws -> A
    ) -> Response<S, A> {
        task(onThrow: onThrow) { _ in try await fn() }
    }

    static func task<S, A>(
        onThrow: @escaping (Error) -> A,
        _ fn: @escaping (S) async throws -> A
    ) -> Response<S, A> {
        Reader<(S, A), InternalResponse<S, A>> { (state, _) in
            InternalResponse.throwableTask(
                onThrow: onThrow,
                throwableFn: { try await fn(state) }
            )
        }
    }

    /// forEach variants: runs a task for each `SS`, an extra variant exists to also take the index of the `SS`

    static func task<S, A, SS>(
        forEach: [SS],
        _ fn: @escaping () async -> A
    ) -> [Response<S, A>] {
        task(forEach: forEach) { _, _ in await fn() }
    }

    static func task<S, A, SS>(
        forEach: [SS],
        _ fn: @escaping (S) async -> A
    ) -> [Response<S, A>] {
        task(forEach: forEach) { _, s in await fn(s) }
    }

    static func task<S, A, SS>(
        forEach: [SS],
        _ fn: @escaping (Int, S) async -> A
    ) -> [Response<S, A>] {
        forEach.indices.map { i in task { await fn(i, $0) } }
    }

    /// onThrow + forEach variants
     
    static func task<S, A, SS>(
        onThrow: @escaping (Error) -> A,
        forEach: [SS],
        _ fn: @escaping () async throws -> A
    ) -> [Response<S, A>] {
        task(onThrow: onThrow, forEach: forEach) { _, _ in try await fn() }
    }

    static func task<S, A, SS>(
        onThrow: @escaping (Error) -> A,
        forEach: [SS],
        _ fn: @escaping (S) async throws -> A
    ) -> [Response<S, A>] {
        task(onThrow: onThrow, forEach: forEach) { _, s in try await fn(s) }
    }

    static func task<S, A, SS>(
        onThrow: @escaping (Error) -> A,
        forEach: [SS],
        _ fn: @escaping (Int, S) async throws -> A
    ) -> [Response<S, A>] {
        forEach.indices.map { i in task(onThrow: onThrow) { try await fn(i, $0) } }
    }

}
