import Foundation
import SwiftUI

public typealias Feature = FeatureUpdate & FeatureView

public protocol FeatureUpdate {
    associatedtype State
    associatedtype Action
    
    @arrayBuilder<Response<State, Action>>
    static func update(action: Action) -> [Response<State, Action>]
}

public protocol FeatureView {
    associatedtype State
    associatedtype Action
    
    static func view(state: State) -> View<Action>
}

public extension FeatureUpdate {
    @arrayBuilder<Response<State, Action>>
    static func update(_ action: Action) -> [Response<State, Action>] {
        update(action: action)
    }
}

public extension FeatureView {
    static func view(_ state: State) -> View<Action> {
        view(state: state)
    }
}

// public extension FeatureView {
//    static func view(_ state: State) -> ViewF {
//        view(state: state)
//    }
//    
//    static func view<A>(_ state: State, _ actionPrism: Prism<A, Action>) -> View<A> {
//        view(state).scoped(actionPrism)
//    }
//    
//    static func view<A>(_ states: [State], _ actionPrism: Prism<A, (Int, Action)>) -> [View<A>] {
//        states.enumerated().map { index, state in
//            Reader { sendParentAction in
//                view(state).runReader { action in
//                    sendParentAction(actionPrism.up((index, action)))
//                }
//            }
//        }
//    }
//    
//    static func view<A>(_ state: State?, _ actionPrism: Prism<A, Action>) -> View<A>? {
//        state.map { view($0, actionPrism) }
//    }
// }
