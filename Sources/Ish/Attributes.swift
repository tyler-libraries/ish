import SwiftUI


public enum InnerOuter<A> {
  case inner(A)
  case outer(A)
}

public enum Outer<A> {
  case outer(A)
}

prefix operator .%

public prefix func .%<A: Attribute, V: SwiftUI.View>(
  _ change: @escaping (AnyView) -> V
) -> A {
  A { AnyView(change($0)) }
}
public prefix func .%(
  _ change: @escaping (Image) -> Image
) -> ImageAttribute {
  ImageAttribute { change($0) }
}
public class Attribute {
  let change: (AnyView) -> AnyView
  
  required init(_ change: @escaping (AnyView) -> AnyView) {
    self.change = change
  }
  
  public static func attribute<V: SwiftUI.View> (
    _ change: @escaping (AnyView) -> V
  ) -> Self {
    .%(change)
  }
  public static func custom<V: SwiftUI.View> (
    _ change: @escaping (AnyView) -> V
  ) -> Self {
    .%(change)
  }
  
  public static func aspectRatio(contentMode: ContentMode) -> Self {
    attribute { $0.aspectRatio(contentMode: contentMode) }
  }
  
  public static func `if`<A: Attribute>(
    _ check: Bool,
    _ attribute: A
  ) -> A {
    if check {
      return attribute
    } else {
      return A { AnyView($0) }
    }
  }
  
  public static func dimBackground() -> Self {
    Self { view in AnyView(ZStack {
      Rectangle().fill(Color.black.opacity(0.4)).ignoresSafeArea()
      view
    })}
  }
  
  public static func frame(
    minWidth: CGFloat? = nil,
    idealWidth: CGFloat? = nil,
    maxWidth: CGFloat? = nil,
    minHeight: CGFloat? = nil,
    idealHeight: CGFloat? = nil,
    maxHeight: CGFloat? = nil,
    alignment: Alignment = Alignment.center
  ) -> Self {
    Self { AnyView($0.frame(
      minWidth: minWidth,
      idealWidth: idealWidth,
      maxWidth: maxWidth,
      minHeight: minHeight,
      idealHeight: idealHeight,
      maxHeight: maxHeight,
      alignment: alignment
    ))
    }
  }

  public static func accentColor(_ color: Color) -> Self {
    attribute { $0.accentColor(color) }
  }
  
  public static func disabled(_ bool: Bool) -> Self {
    Self { AnyView($0.disabled(bool)) }
  }
  
  public static func width(_ n: CGFloat) -> Self {
    Self { AnyView($0.frame(width: n)) }
  }
  
  public static func height(_ n: CGFloat) -> Self {
    Self { AnyView($0.frame(height: n)) }
  }
  
  public static func alignment(_ alignment: Alignment) -> Self {
    Self { view in AnyView(view.frame(alignment: alignment)) }
  }
  
  public static func padding(_ edges: Edge.Set = .all, _ length: CGFloat? = nil) -> Self {
    Self { AnyView($0.padding(edges, length)) }
  }
  
  public static func background<S: ShapeStyle>(_ s: S) -> Self {
    Self { AnyView($0.background(s)) }
  }
  
  public static func foregroundColor(_ color: Color) -> Self {
    Self { AnyView($0.foregroundColor(color)) }
  }
  
  public static func font(_ font: Font?) -> Self {
    Self { AnyView($0.font(font)) }
  }
}

public enum ForEachAttribute {
    case onMove(((IndexSet, Int) -> Void)?)
}

public class ImageAttribute : Attribute {
  let imageChange: (Image) -> Image
  
  public required init(_ change: @escaping (AnyView) -> AnyView) {
    self.imageChange = { $0 }
    super.init(change)
  }
  
  init(_ imageChange: @escaping (Image) -> Image) {
    self.imageChange = imageChange
    super.init({ $0 })
  }
  
  public static func resizable() -> ImageAttribute {
    ImageAttribute { $0.resizable() }
  }
  
  public static func attribute(
    _ change: @escaping (Image) -> Image
  ) -> ImageAttribute {
    ImageAttribute(change)
  }
  public static func custom(
    _ change: @escaping (Image) -> Image
  ) -> ImageAttribute {
    ImageAttribute(change)
  }

}

public class VStackAttribute: Attribute {
  let alignment: HorizontalAlignment?
  let spacing: CGFloat?
  
  required init(_ change: @escaping (AnyView) -> AnyView) {
    self.alignment = nil
    self.spacing = nil
    super.init(change)
  }
  
  init(alignment: HorizontalAlignment? = nil, spacing: CGFloat? = nil) {
    self.alignment = alignment
    self.spacing = spacing
    super.init({ $0 })
  }
  
  public static func spacing(_ n: CGFloat) -> VStackAttribute {
    VStackAttribute(spacing: n)
  }
  
  public static func vstackAlignment(_ a: HorizontalAlignment) -> VStackAttribute {
    VStackAttribute(alignment: a)
  }
}


public class HStackAttribute: Attribute {
  let alignment: VerticalAlignment?
  let spacing: CGFloat?
  
  required init(_ change: @escaping (AnyView) -> AnyView) {
    self.alignment = nil
    self.spacing = nil
    super.init(change)
  }
  
  init(alignment: VerticalAlignment? = nil, spacing: CGFloat? = nil) {
    self.alignment = alignment
    self.spacing = spacing
    super.init({ $0 })
  }
  
  public static func spacing(_ n: CGFloat) -> HStackAttribute {
    HStackAttribute(spacing: n)
  }
  
  public static func hstackAlignment(_ a: VerticalAlignment) -> HStackAttribute {
    HStackAttribute(alignment: a)
  }
}

public class ZStackAttribute: Attribute {
  let alignment: Alignment?
  
  required init(_ change: @escaping (AnyView) -> AnyView) {
    self.alignment = nil
    super.init(change)
  }
  
  init(alignment: Alignment? = nil) {
    self.alignment = alignment
    super.init({ $0 })
  }
  
  public static func zstackAlignment(_ a: Alignment) -> ZStackAttribute {
    ZStackAttribute(alignment: a)
  }
}

//public class ListAttribute: Attribute {
//  let listChange: (List<SelectionValue, Content>) -> List
//  
//  public required init(_ change: @escaping (AnyView) -> AnyView) {
//    self.listChange = { $0 }
//    super.init(change)
//  }
//  
//  init(_ listChange: @escaping (List) -> List) {
//    self.listChange = listChange
//    super.init({ $0 })
//  }
//  
//  public static func attribute(
//    _ change: @escaping (Image) -> Image
//  ) -> ListAttribute {
//    ListAttribute(change)
//  }
//}
