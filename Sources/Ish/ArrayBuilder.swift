@resultBuilder
public struct arrayBuilder<A> {
    public static func buildBlock(_ components: [A]...) -> [A] {
        components.flatMap { $0 }
    }
    
    public static func buildExpression(_ expression: A) -> [A] {
        [expression]
    }
    
    public static func buildExpression(_ expression: [A]) -> [A] {
        expression
    }
    
    public static func buildExpression(_ expression: A?) -> [A] {
        expression.map { [$0] } ?? []
    }
    
    public static func buildOptional(_ components: [A]?) -> [A] {
        components ?? []
    }
    
    public static func buildEither(first components: [A]) -> [A] {
        components
    }
    
    public static func buildEither(second components: [A]) -> [A] {
        components
    }
    
    public static func buildArray(_ components: [[A]]) -> [A] {
        components.flatMap { $0 }
    }
}

public func buildArray<A>(@arrayBuilder<A> xs: () -> [A]) -> [A] {
    xs()
}

public extension Array {
    func mapAtIndex(_ i: Int, _ f: (Element) -> Element) -> [Element] {
        enumerated().map { i == $0 ? f($1) : $1 }
    }
    
    func replaceAtIndex(_ i: Int, _ element: Element) -> [Element] {
        mapAtIndex(i) { _ in element }
    }

    func get(safe index: Int) -> Element? {
        return self.indices.contains(index) ? self[index] : nil
    }
    subscript(safe index: Int) -> Element? {
        return self.indices.contains(index) ? self[index] : nil
    }
}

extension Array {
    func modifications<S, A>() -> [(S) -> S]
        where Element == InternalResponse<S, A> {
        compactMap {
            switch $0 {
            case let .modify(m): return m
            default: return nil
            }
        }
    }
}
