import SwiftUI
// import Reader

public func fromSwiftUI<A, V: ViewSUI>(
  _ view: @escaping (@escaping (A) -> ()) -> V
) -> View<A> {
    Reader { view($0) }.toView()
}

public func fromSwiftUI<A, V: ViewSUI>(
  _ view: @escaping (@escaping (A) -> ()) -> V
) -> SpecificView<A, V> {
    Reader { view($0) }
}

public func fromSwiftUI<A, V: ViewSUI>(
  _ view: @escaping () -> V
) -> SpecificView<A, V> {
    pure(view())
}

public func fromSwiftUI<A, V: ViewSUI>(
  _ view: @escaping () -> V
) -> View<A> {
    pure(view()).toView()
}


/// `context` View variant. `Show Quick Help` for more information.
///
/// There are 4 overloads to this function:
/// 1. 2 return `View<S, A>`
/// 2. 2 return `SpecificView<S, A, V>`
///
/// Normally, you shouldn't have to worry about the latter group, and the inference algorithm should generally pick the former for you because you'll be returning `Ish.View`s from your functions.
///
/// In both of those groups, the 2 variants are as follows:
/// 1. `fromSwiftUI { context in `
/// 3. `fromSwiftUI { `
///
/// Here is an example:
/// ```
/// fromSwiftUI { context in
///   Button {
///     context.sendAction(.someCaseOnActionEnum)
///   } label: { Text("-") }
///
///   myIshView().toSwiftUI(context)
/// }
/// ```
/// You could also construct a separate function and pass that function in directly:
/// ```
/// func mySwiftUIView(
///   sendAction: @escaping (Action) -> (),
///   context: Context<State, Action>
/// ) -> some SwiftUI.View {
///   // your swiftui view
/// }
/// // usage elsewhere
/// fromSwiftUI(mySwiftUIView)
/// ```
///
/// For another example, check `Examples/Concepts/SwiftUIInterop.swift` (read the README.md first)
///
/// - Parameters:
///     - view: A function that takes a `sendAction` function, a `context` for constructing SwiftUI.View from Ish.View, and returns the SwiftUI.View
///
/// - Returns: An Ish.View constructed from the SwiftUI.View
