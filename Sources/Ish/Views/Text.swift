import Bow
import SwiftUI

public func text<A>(
  value: CustomStringConvertible,
  attributes: [Attribute] = []
) -> View<A> {
    pure(setAttributes(Text(value.description), attributes))
}

public func text<A>(
  _ value: CustomStringConvertible,
  attributes: [Attribute] = []
) -> View<A> {
  text(value: value, attributes: attributes)
}

//public func text<S, A>(
//  value: String,
//) -> SpecificView<S, A, Text> {
//
//  text(value: value, attributes: attributes)
//}
//
//public func text<S, A>(
//  _ value: String,
//) -> SpecificView<S, A, Text> {
//  text(value: value, attributes: attributes)
//}

public func label<A>(
  _ title: String,
  imageName: String,
  attributes: [Attribute] = []
) -> View<A> {
    pure(setAttributes(Label(title, systemImage: imageName), attributes))
}

public func textField<A>(
  placeholder: String,
  value: String,
  onChange: @escaping (String) -> A,
  attributes: [Attribute] = []
) -> View<A> {
    Reader { send in
        let binding: Binding<String> = Binding(
            get: { value },
            set: { (str: String) -> Void in
                send(onChange(str))
            }
        )
        // https://stackoverflow.com/questions/70734379/why-is-didset-called-twice-on-the-textfield-binding-in-swiftui
        let textField = TextField(placeholder, text: binding).textFieldStyle(.plain)
        
        return setAttributes(textField, attributes)
    }.toView()
}

// TODO: SecureField
// TODO: TextEditor
