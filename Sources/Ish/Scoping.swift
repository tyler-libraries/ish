import Foundation

public extension Reader {
//    func scoped<SSS, AAA, SS, AA>(_ stateLens: Lens<SSS, SS>, _ actionPrism: Prism<AAA, AA>) -> Response<SSS, AAA> {
//        let reader = Reader { (s: SSS, a: AAA) -> (Response<SSS, AAA>, SSS, AAA) in
//            
//        }
//    }
    // MARK: Response Extensions
    // TODO: create third?
    func scoped<T, R, TT, RR>(
        _ stateLens: Lens<T, TT>,
        _ actionPrism: Prism<R, RR>
    ) -> Response<T, R> where E == (TT, RR), A == InternalResponse<TT, RR> {
        Reader<(T, R), (Response<T, R>, T, R)> { (s: T, a: R) -> (Response<T, R>, T, R) in
            let state: TT = stateLens.get(s)
            let noop: Response<T, R> = .noOp()
            guard let action: RR = actionPrism.down(a) else { return (noop, s, a) }
            let newResponse: Response<T, R> = switch runReader((state, action)) {

            case let .modify(mm):
                .modify(stateLens %~ mm)
            case let .task(fn):
                .task { await actionPrism.up(fn()) }
            case let .throwableTask(onThrow, fn):
                .task(onThrow: { actionPrism.up(onThrow($0)) }) { try await fn() |> actionPrism.up }
            case let .sideTask(fn):
                sideTask(fn)
            case let .throwableSideTask(onThrow, fn):
                sideTask(onThrow: onThrow >>> actionPrism.up, fn)
            case .noOp:
                .noOp()
            }
            return (newResponse, s, a)
        }.map { $0.runReader(($1, $2)) }
    }
    
    func scoped<T, R, TT, RR>(_ stateLens: Lens<T, TT?>, _ actionPrism: Prism<R, RR>) -> Response<T, R> where E == (TT, RR), A == InternalResponse<TT, RR> {
        Reader<(T, R), (Response<T, R>, T, R)> { (s: T, a: R) -> (Response<T, R>, T, R) in
            let noop: Response<T, R> = .noOp()
            guard let state: TT = stateLens.get(s), let action: RR = actionPrism.down(a) else {
                return (noop, s, a)
            }
            let newResponse: Response<T, R> = switch runReader((state, action)) {
            case let .modify(mm):
                .modify(stateLens %~ { $0.map(mm) })
            case let .task(fn):
                Response<T, R>.task { await actionPrism.up(fn()) }
            case let .throwableTask(onThrow, fn):
                .task(onThrow: { actionPrism.up(onThrow($0)) }) { try await fn() |> actionPrism.up }
            case let .sideTask(fn):
                sideTask(fn)
            case let .throwableSideTask(onThrow, fn):
                sideTask(onThrow: onThrow >>> actionPrism.up, fn)
            case .noOp:
                .noOp()
            }
            return (newResponse, s, a)
        }.map { $0.runReader(($1, $2)) }
    }
//        let rpotato: Reader<(SSS, AAA), (Response<SSS, AAA>, SSS, AAA)> =
//            Reader { (s: SSS, a: AAA) -> (Response<SSS, AAA>, SSS, AAA) in
//            let state: SS = stateLens.get(s)
//            let noop: Response<SSS, AAA> = noOp()
//            guard let action: AA = actionPrism.down(a) else { return (noop, s, a) }
//                
//            let response: Response<SSS, AAA> = switch runReader((state, action)) {
//            case let .lastItemAction(fn):
//                lastItemAction { await fn() |> actionPrism.up }
//            case let .modify(mm):
//                modify(stateLens %~ mm)
//            case let .task(fn):
//                task { await actionPrism.up(fn()) }
//            case let .throwableTask(onThrow, fn):
//                task(onThrow: { actionPrism.up(onThrow($0)) }) { try await fn() |> actionPrism.up }
//            case let .sideTask(fn):
//                sideTask(fn)
//            case let .throwableSideTask(onThrow, fn):
//                sideTask(onThrow: onThrow >>> actionPrism.up, fn)
//            case .noOp:
//                noOp()
//            }
//            return (response, s, a)
//        }
//            
//        return rpotato.map { (response: Response<SSS, AAA>, s: SSS, a: AAA) -> InternalResponse<SSS, AAA> in
//            response.runReader((s, a))
//        }
//    }
    
    // View Extensions
    func scoped<B, BB>(_ actionPrism: Prism<B, BB>) -> View<B>
    where E == (BB) -> (), A == AnyViewSUI {
        Reader<(B) -> (), AnyViewSUI> { sendParentAction in
            runReader { action in
                sendParentAction(actionPrism.up(action))
            }
        }
    }
}

func doTheShit<SSS, AAA>(response: Response<SSS, AAA>, s: SSS, a: AAA) {
    response.runReader((s, a))
}

public extension Array {
    // MARK: Responses Extensions
    func scoped<S, SS, A, AA>(_ stateLens: Lens<S, SS>, _ actionPrism: Prism<A, AA>) -> [Response<S, A>]
    where Element == Response<SS, AA> {
        map { (response: Response<SS, AA>) -> Response<S, A> in
            Reader { (s: S, a: A) -> (Response<S, A>, S, A) in
                let state: SS = stateLens.get(s)
                let noop: Response<S, A> = noOp()
                guard let action: AA = actionPrism.down(a) else { return (noop, s, a) }
                let newResponse: Response<S, A> = switch response.runReader((state, action)) {
                case let .modify(mm):
                    modify(stateLens %~ mm)
                case let .task(fn):
                    task { await actionPrism.up(fn()) }
                case let .throwableTask(onThrow, fn):
                    task(onThrow: { actionPrism.up(onThrow($0)) }) { try await fn() |> actionPrism.up }
                case let .sideTask(fn):
                    sideTask(fn)
                case let .throwableSideTask(onThrow, fn):
                    sideTask(onThrow: onThrow >>> actionPrism.up, fn)
                case .noOp:
                    noOp()
                }
                return (newResponse, s, a)
            }.map { $0.runReader(($1, $2)) }
        }
    }
    
    func scoped<S, SS, A, AA>(_ stateLens: Lens<S, SS?>, _ actionPrism: Prism<A, AA>) -> [Response<S, A>]
    where Element == Response<SS, AA> {
        map { response in
            Reader { (s, a) in
                let noop: Response<S, A> = noOp()
                guard let state = stateLens.get(s), let action = actionPrism.down(a) else {
                    return (noop, s, a)
                }
                
                let response: Response<S, A> = switch response.runReader((state, action)) {
                case let .modify(mm):
                    modify(stateLens %~ { $0.map(mm) })
                case let .task(fn):
                    task { await actionPrism.up(fn()) }
                case let .throwableTask(onThrow, fn):
                    task(onThrow: onThrow >>> actionPrism.up) { try await actionPrism.up(fn()) }
                case let .sideTask(fn):
                    sideTask { await fn() }
                case let .throwableSideTask(onThrow, fn):
                    sideTask(onThrow: onThrow >>> actionPrism.up, fn)
                case .noOp:
                    noOp()
                }
                return (response, s, a)
            }.map { $0.runReader(($1, $2)) }
        }
    }
    
    func scoped<S, SS, A, AA>(_ stateLens: Lens<S, [SS]>, _ actionPrism: Prism<A, (Int, AA)>) -> [Response<S, A>]
    where Element == Response<SS, AA> {
        map { response in
            Reader { (s, a) in
                let allState = stateLens.get(s)
                let noop: Response<S, A> = noOp()
                guard let (i, action) = actionPrism.down(a), let state = allState.get(safe: i) else { return (noop, s, a) }
                func actionPrismUp(_ a: AA) -> A {
                    actionPrism.up((i, a))
                }
                // guard let state = allState.get(safe: i), let action = actionPrism.down(a) else { return (noop, s) }
                let response: Response<S, A> = switch response.runReader((state, action)) {
                case let .modify(m):
                    modify(stateLens %~ { $0.mapAtIndex(i, m) })
                case let .task(fn):
                    task { await fn() |> actionPrismUp }
                case let .throwableTask(onThrow, fn):
                    task(onThrow: onThrow >>> actionPrismUp) { try await fn() |> actionPrismUp }
                case let .sideTask(fn):
                    sideTask(fn)
                case let .throwableSideTask(onThrow, fn):
                    sideTask(onThrow: { actionPrism.up((i, onThrow($0))) }, fn)
                case .noOp:
                    noOp()
                }
                return (response, s, a)
            }.map { $0.runReader(($1, $2)) }
        }
    }
    
    // View Extensions
    func scoped<A, AA>(_ actionPrism: Prism<A, (Int, AA)>) -> [View<A>]
    where Element == View<AA> {
        enumerated().map { index, view in
            Reader { sendParentAction in
                view.runReader { action in
                    sendParentAction(actionPrism.up((index, action)))
                }
            }
        }
//        states.enumerated().map { index, state in
//            Reader { sendParentAction in
//                view(state).runReader { action in
//                    sendParentAction(actionPrism.up((index, action)))
//                }
//            }
//        }
    }
}

// TODO: reason out, do I need an optional view scoped? (is .map sufficient?)
