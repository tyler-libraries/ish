import Foundation
import Combine
import SwiftUI
// import Reader

func handleEvent<S, A>(_ a: A, _ app: AppState<S, A>, _ _responses: [Response<S, A>]) {
    
    let responses: [InternalResponse<S, A>] = if let s = app.state {
        _responses.map { $0.runReader((s, a)) }
    } else {
        []
    }
    
    for m in responses.modifications() {
        // StateQueue.asyncMain {
        DispatchQueue.main.async {
            app.state = app.state.map(m)
        }
    }
    let err = "handle event called for nil app state, this means a static view is used in a real app loop, static views are only for previews or other extremely simple cases"
//    @Sendable func handle(_ action: (S) async throws -> A) async {
//        if let state = app.state {
//            await handleEvent(app, app.update(state, action(state)))
//        } else {
//            preconditionFailure("handle event called for nil app state, this means a static view is used in a real app loop, static views are only for previews or other extremely simple cases")
//        }
//    }
    for response in responses {
        StateQueue.asyncMain {
            Task {
                switch response {
                case let .task(fn):
                    let a = await fn()
                    handleEvent(a, app, app.update(a))

                case let .sideTask(fn): await fn()
                    // TODO: remove if let checks here
                case let .throwableTask(onThrow, fn):
                    do {
                        if let s = app.state {
                            let a = try await fn()
                            handleEvent(a, app, app.update(a))
                        } else {
                            preconditionFailure(err)
                        }
                        // try await handle(fn)
                    } catch {
                        if let s = app.state {
                            let a = onThrow(error)
                            handleEvent(a, app, app.update(a))
                        } else {
                            preconditionFailure(err)
                        }
                        // handle(onThrow(error))
                    }
   
                case let .throwableSideTask(onThrow, fn):
                    do {
                        try await fn()
                    } catch {
                        if let s = app.state {
                            let a = onThrow(error)
                            handleEvent(a, app, app.update(a))
                        } else {
                            preconditionFailure(err)
                        }
                    }
                case .modify(_): ()
                case .noOp:
                    ()
                }
            }
        }
    }
}
public func staticView<A, BuildState>(
    _ state: BuildState,
    _ buildUI: (BuildState) -> View<A>
) -> AnyView {
    buildUI(state).toSwiftUI { _ in }
}
//public protocol ViewP<SS, AA> {
//    associatedtype S
//    associatedtype A
//    associatedtype PS
//    associatedtype PA
//    associatedtype SS
//    associatedtype AA
//    
//    func toSwiftUI(_ context: Context<S, A, PS, PA, SS, AA>) -> AnyView
//    
//    func applyAttributes(_ attributes: [Attribute]) -> Self
//}


class AppState<S, A>: ObservableObject {
    @Published var state: S? = nil
    let update: (A) -> [Response<S, A>]
    
    init(state: S?, update: @escaping (A) -> [Response<S, A>]) {
        self.state = state
        self.update = update
    }
}

struct AppStateConsumer<S, A>: ViewSUI {
    @ObservedObject var app: AppState<S, A>
    let stateConsumer: (S) -> AnyView
    
    var body: some ViewSUI {
        if let state = app.state {
            //StateQueue.sync("stateConsumer") {
            return stateConsumption(state)
            //}
        } else {
            preconditionFailure("The only time the appstate could be nil is during the use of static views, only use static views for previews or really simple testing.")
        }
    }
    
    func stateConsumption(_ state: S) -> AnyView {
        // print("state consumption")
        return stateConsumer(state)
    }
}

struct StateQueue {
    private static let queue = DispatchQueue(label: "com.Ish.StateQueue")
    
    static func `asyncMain`(fn: @escaping () -> ()) {
        queue.async {
            DispatchQueue.main.sync {
                fn()
            }
        }
    }
    
    static func `async`(_ label: String, _ fn: @escaping () -> ()) {
        // print("\(label.take(50)) queued")
        queue.async {
            // print("\(label.dropLast(abs(label.count - 50))) executed")
            fn()
        }
    }
    
    static func sync<A>(_ label: String, _ fn: @escaping () -> A) -> A {
        // print("\(label.take(50)) queued")
        return queue.sync {
            // print("\(label.dropLast(abs(label.count - 50))) executed")
            return fn()
        }
    }
}

extension String {
    func take(_ n: Int) -> String {
        if n > self.count {
            return self
        } else {
            return "\(self.dropLast(self.count - n))"
        }
    }
}
public struct IshDebug {
    public static var logging: Bool = false
    public static var loggingMechanism: (String) -> () = { message in
        if logging {
            print(message)
        }
    }
    
    static var log = loggingMechanism
}

public func start<S, A>(
    view: @escaping (S) -> View<A>,
    update: @escaping (A) -> [Response<S, A>],
    initialState state: S,
    initialAction: A? = nil
) -> some ViewSUI {
    let app: AppState<S, A> = AppState(state: state, update: update)

    let appConsumer = AppStateConsumer(
        app: app,
        stateConsumer: { state in
            AnyView(view(state).runReader { action in
                handleEvent(action, app, update(action))
            })
        }
    )
    
    initialAction.map { handleEvent($0, app, update($0)) }
    
    return appConsumer
}

/// TODO: Create the following subview variants
/// - subviews: WritableKeyPath<S, [SS]>   & CasePath<A, AA>
/// - subviews: WritableKeyPath<S, [SS]?> & CasePath<A, AA>
/// - subviews: WritableKeyPath<S, [SS]>   & CasePath <A, (Int, AA)>
/// - subviews: WritableKeyPath<S, [SS]?> & CasePath<A, (Int, AA)>
/// - subview: WritableKeyPath<S, SS>    & CasePath<A, AA>
/// - subview: WritableKeyPath<S, SS?>  & CasePath<A, AA>
/// They should each move from the buildUI context of
/// `View<S, A, PS, PA, SS, AA>`
/// to
/// `View<S, A, PPS, PPA, PS, PA>`
/// When they're implemented, implement `asSubview(s)` in terms of them.
