import SwiftUI
import Foundation
// import Reader


public typealias ViewSUI = SwiftUI.View
public typealias AnyViewSUI = SwiftUI.AnyView
public typealias ViewIsh = Ish.View
public typealias App = SwiftUI.App
public typealias Scene = SwiftUI.Scene
public typealias WindowGroup = SwiftUI.WindowGroup
public typealias PreviewProvider = SwiftUI.PreviewProvider


public typealias View<A> = Reader<(A) -> (), AnyView>
public typealias SpecificView<A, V: ViewSUI> = Reader<(A) -> (), V>

public extension Reader {
    // MARK: View & Specific View Extensions (was in both structs)
    func toSwiftUI<AA>(_ context: E) -> A
        where E == (AA) -> (), A: SwiftUI.View {
        runReader(context)
    }

    // MARK: View Extensions
    func applyAttributes<AA>(_ attributes: [Attribute]) -> Self
        where E == (AA) -> (), A == AnyView {
        map { setAttributes($0, attributes) }
    }
    
    
    // MARK: SpecificView Extensions
    func toView<AA>() -> View<AA>
        where E == (AA) -> (), A: SwiftUI.View {
        map { AnyView($0) }
    }
}


public func emptyView<A>() -> View<A> {
    fromSwiftUI { EmptyView() }
}

public enum ParentOrChild {
    case parentState
    case childState
}

public func vstack<A>(
    attributes: [VStackAttribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, []),
    @arrayBuilder<View<A>> fromBuilder: () -> [View<A>]
) -> View<A> {
    vstackFromList(fromBuilder(), attributes: attributes, forEachAttributes: forEachAttributes)
}

public func vstackFromList<A>(
    _ content: [View<A>],
    attributes: [VStackAttribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, [])
) -> View<A> {
    let alignment = attributes.compactMap { $0.alignment }.first
    let spacing   = attributes.compactMap { $0.spacing }.first
    
    return someStack(
        content,
        stackKind: { v in AnyView(VStack(
            alignment: alignment ?? HorizontalAlignment.center,
            spacing: spacing
        ) { v() }) },
        stackKindStr: "VStack",
        attributes,
        forEachAttributes
    )
}

public func hstack<A>(
    attributes: [HStackAttribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, []),
    @arrayBuilder<View<A>> fromBuilder: () -> [View<A>]
) -> View<A> {
    hstackFromList(fromBuilder(), attributes: attributes, forEachAttributes: forEachAttributes)
}

public func hstackFromList<A>(
    _ content: [View<A>],
    attributes: [HStackAttribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, [])
) -> View<A> {
    let alignment = attributes.compactMap { $0.alignment }.first
    let spacing   = attributes.compactMap { $0.spacing }.first
    
    return someStack(
        content,
        stackKind: { v in AnyView(HStack(
            alignment: alignment ?? VerticalAlignment.center,
            spacing: spacing
        ) { v() }) },
        stackKindStr: "HStack",
        attributes,
        forEachAttributes
    )
}

public func zstack<A>(
    attributes: [ZStackAttribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, []),
    @arrayBuilder<View<A>> fromBuilder: () -> [View<A>]
) -> View<A> {
    zstackFromList(fromBuilder(), attributes: attributes, forEachAttributes: forEachAttributes)
}

public func zstackFromList<A>(
    _ content: [View<A>],
    attributes: [ZStackAttribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, [])
) -> View<A> {
    let alignment = attributes.compactMap { $0.alignment }.first
    
    return someStack(
        content,
        stackKind: { v in AnyView(ZStack(
            alignment: alignment ?? Alignment.center
        ) { v() }) },
        stackKindStr: "ZStack",
        attributes,
        forEachAttributes
    )
}

public func group<A>(
    attributes: [Attribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, []),
    @arrayBuilder<View<A>> fromBuilder: () -> [View<A>]
) -> View<A> {
    groupFromList(fromBuilder(), attributes: attributes, forEachAttributes: forEachAttributes)
}

public func groupFromList<A>(
    _ content: [View<A>],
    attributes: [Attribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, [])
) -> View<A> {
    someStack(
        content,
        stackKind: { v in AnyView(Group { v() }) },
        stackKindStr: "Group",
        attributes,
        forEachAttributes
    )
}

public func list<A>(
    attributes: [Attribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, []),
    @arrayBuilder<View<A>> fromBuilder: () -> [View<A>]
) -> View<A> {
    listFromList(fromBuilder(), attributes: attributes, forEachAttributes: forEachAttributes)
}

public func listFromList<A>(
    _ content: [View<A>],
    attributes: [Attribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, [])
) -> View<A> {
    someStack(
        content,
        stackKind: { v in AnyView(List { v() }) },
        stackKindStr: "List",
        attributes,
        forEachAttributes
    )
}

public func someStack<A, V: ViewSUI>(
    _ stackKind: @escaping (AnyView) -> V,
    attributes: [Attribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, []),
    @arrayBuilder<View<A>> fromBuilder: () -> [View<A>]
) -> View<A> {
    someStackFromList(stackKind, fromBuilder(), attributes: attributes, forEachAttributes: forEachAttributes)
}


public func someStackFromList<A, V: ViewSUI>(
    _ stackKind: @escaping (AnyView) -> V,
    _ content: [View<A>],
    attributes: [Attribute] = [],
    forEachAttributes: (ForEachAttribute?, [Attribute]) = (nil, [])
) -> View<A> {
    someStack(
        content,
        stackKind: { v in AnyView(stackKind(v())) },
//        stackKind: { v in AnyView(List { v() }) },
        stackKindStr: "SomeStack",
        attributes,
        forEachAttributes
    )
}
func someStack<A>(
    _ content: [View<A>],
    stackKind: @escaping (@escaping () -> AnyView) -> AnyView,
    stackKindStr: String,
    _ attributes: [Attribute],
    _ forEachAttributes: (ForEachAttribute?, [Attribute])
) -> View<A> {
    Reader { internalState in
        let stack = stackKind {
            let (terminatingAttr, fAttributes) = forEachAttributes
            switch terminatingAttr {
            case .onMove(let fn):
                return setAttributes(AnyView(ForEach(content.indices, id: \.self) {
                    content[$0].toSwiftUI(internalState)
                }.onMove(perform: fn)), fAttributes)
            case nil:
                return setAttributes(AnyView(ForEach(content.indices, id: \.self) {
                    content[$0].toSwiftUI(internalState)
                }), fAttributes)
            }
        }
        return setAttributes(stack, attributes)
    }.toView()
}

public func button<A>(
    _ action: A?,
    _ label: String,
    attributes: [Attribute] = []
) -> View<A> {
    button(action: action, label: text(label), attributes: attributes)
}

public func button<A>(
    action: A?,
    label: String,
    attributes: [Attribute] = []
) -> View<A> {
    button(action: action, label: text(label), attributes: attributes)
}

public func button<A>(
    _ action: A?,
    _ label: View<A>,
    attributes: [Attribute] = []
) -> View<A> {
    button(action: action, label: label, attributes: attributes)
}


public func button<A>(
    action: A?,
    label: View<A>,
    attributes: [Attribute] = []
) -> View<A> {
    Reader { sendAction in
        let button = Button {
            action.map(sendAction)
        } label: {
            label.runReader(sendAction)
        }
        return setAttributes(button, attributes)
    }.toView()
}

public func image<A>(
    name: String,
    attributes: [ImageAttribute] = []
) -> View<A> {
    pure(setAttributes(Image(name), attributes)).toView()
}

#if canImport(UIKit)
import UIKit

public func image<A>(
    _ image: UIImage,
    attributes: [ImageAttribute] = []
) -> View<A> {
    pure(setAttributes(Image(uiImage: image), attributes))
}
#endif

public func divider<A>() -> View<A> {
    fromSwiftUI { Divider() }
}
public func spacer<A>() -> View<A> {
    fromSwiftUI { Spacer() }
}

func setAttributes<V: ViewSUI>(_ view: V, _ attributes: [Attribute]) -> AnyView {
    let imageAttributes = attributes.compactMap { $0 as? ImageAttribute }
    let view1: AnyView? = (view as? Image).map { image in
        AnyView(imageAttributes.foldLeft(image) { (acc, e) in
            e.imageChange(acc)
        })
    }
    
    
    return attributes.foldLeft(view1 ?? AnyView(view)) {
        $1.change($0)
    }
}

public enum IshAlertButton {
    case `default`(_ label: String, action: (() -> Void)? = {})
    case cancel(_ label: String? = nil, action: (() -> Void)? = {})
    case destructive(_ label: String, action: (() -> Void)? = {})
}

extension IshAlertButton {
    func toSwiftUI() -> Alert.Button {
        switch self {
        case .`default`(let str, let action):
            return Alert.Button.default(Text(str), action: action)
        case let .cancel(.some(str), action):
            return .cancel(Text(str), action: action)
        case .cancel(nil, let action):
            return .cancel(action)
        case let .destructive(str, action):
            return .destructive(Text(str), action: action)
        }
    }
    
    func addAction(newAction: @escaping () -> Void) -> IshAlertButton {
        switch self {
        case let .`default`(str, .some(action)):
            return .default(str) { newAction(); action(); }
        case let .cancel(str, .some(action)):
            return .cancel(str) { newAction(); action(); }
        case let .destructive(str, .some(action)):
            return .destructive(str) { newAction(); action(); }
        default:
            return self
        }
    }
}

public func unsafeSystemAlert<A>(
    title: String,
    message: String? = nil,
    dismissButton: IshAlertButton? = nil
) -> View<A> {
    Reader { os in
        let binding: Binding<Bool> = Binding.init(
            get: { true },
            set: { _ in }
        )
        return AnyView(Color.clear.frame(width: 0, height: 0)
            .alert(isPresented: binding) {
                Alert(title: Text(title), message: message.map { Text($0) },
                      dismissButton: dismissButton?.addAction {
                    
                    // print(binding.wrappedValue)
                    binding.wrappedValue = false
                    binding.wrappedValue = true
                }.toSwiftUI())
            }
        )
    }.toView()
}

public enum AlertButton<A> {
    case ok(A)
    case cancel(A)
    case custom(Color, String, A)
}

public func alert<A>(
    title: String,
    message: String? = nil,
    button: AlertButton<A>,
    cancelButton: AlertButton<A>? = nil,
    dimBackground: Bool
) -> Ish.View<A> {
    func mkButton(_ button: AlertButton<A>, _ send: @escaping (A) -> ()) -> some SwiftUI.View {
        switch button {
        case let .ok(action):
            return mkButton(.custom(.blue, "OK", action), send)
        case let .cancel(action):
            return mkButton(.custom(.blue, "Cancel", action), send)
        case let .custom(color, text, action):
            return Button { send(action) } label: {
                Text(text)
                    .foregroundColor(color)
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
            }.padding(.bottom, 12)
        }
        
    }
    return fromSwiftUI { sendAction in
        ZStack {
            if dimBackground {
                Rectangle()
                    .fill(Color.black.opacity(0.4))
                    .ignoresSafeArea()
                    .onTapGesture {
                        // showAlert = false
                    }
            }
            
            let vstack = VStack {
                Text(title)
                    .font(.headline)
                    .padding(.top)
                    .padding(.bottom, 1)
                
                if let msg = message {
                    Text(msg)
                        .font(.system(size: 14))
                        .padding(.bottom)
                }
                Spacer()
                Divider()
                HStack {
                    if let btn = cancelButton {
                        mkButton(btn, sendAction)
                        Divider().padding(.top, -9)
                    }
                    mkButton(button, sendAction)
                }.frame(height: 40)
            }
                .frame(width: 280, height: 140)
                .background(Color(red:0.876, green:0.891, blue:0.890))
                .cornerRadius(15)
            
            if dimBackground {
                vstack.shadow(radius: 10)
            } else {
                vstack
            }
        }
    }
}
