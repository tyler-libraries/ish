//import Foundation
//import Bow
//
//public func startUpdateOnlyLoop<S, A>(
//    initialState: S,
//    update: @escaping (S, A) -> [Response<S, A>],
//    actions: [A]
//) async -> [S] {
//    let app = AppState(state: initialState, update: { s, a in
//        update(s,a).map { $0.internalResponse }
//    })
//    var states: [S] = [initialState]
//
//    for action in actions {
//        await blockingInternalHandleEvent(app: app, action: action)
//        states.append(app.state)
//    }
//    return states
//}
//
//public func startUpdateOnlyLoop<S, A>(
//    initialState: S,
//    update: @escaping (S, A) -> [Response<S, A>],
//    actionsAndStates: [(A, (S) -> Bool)],
//    assertion: (Bool, String) -> ()
//) async {
//    let actions: [A] = actionsAndStates.map { $0.0 }
//    let expectedStates: [(S) -> Bool] = actionsAndStates.map { $0.1 }
//    let states: [S] = await Array(startUpdateOnlyLoop(
//        initialState: initialState,
//        update: update,
//        actions: actions
//    ).dropFirst(1))
//
//    for (i, (state, expectedState)) in zip(states, expectedStates).enumerated() {
//        let str = "click for more info\nstate[\(i)] was unexpected - found \(state)"
//        assertion(expectedState(state), str)
//    }
//}
//func blockingInternalHandleEvent<S, A>(app: AppState<S, A>, action: A) async {
//  for response in app.update(app.state, action) {
//    switch response {
//    case let .modify(m):
//      DispatchQueue.main.sync {
//        os.state = m(os.state)
//      }
//    case let .task(io):
//      let nextAction = await io()
//      await blockingInternalHandleEvent(os: os, action: nextAction)
//    case let .throwableTask(onThrow, io):
//      var result: A
//      do {
//        result = try await io()
//      } catch {
//        result = onThrow(error)
//      }
//      await blockingInternalHandleEvent(os: os, action: result)
//    case let .sideTask(io):
//      await io()
//    case let .throwableSideTask(onThrow, io):
//      do {
//        try await io()
//      } catch {
//        await blockingInternalHandleEvent(os: os, action: onThrow(error))
//      }
//    }
//  }
//}
