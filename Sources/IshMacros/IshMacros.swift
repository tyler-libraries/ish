//@attached(member, names: arbitrary)
//public macro makeLensesz() = #externalMacro(module: "IshMacrosMacros", type: "MakeStaticLensesMacro")
//
//@attached(peer, names: arbitrary)
//public macro makeLensesPeer() = #externalMacro(module: "IshMacrosMacros", type: "MakeLensesMacro")
//
///// Ignore this unless you know what you're doing
//@attached(peer, names: arbitrary)
//public macro makeLensesPeerNonStatic() = #externalMacro(module: "IshMacrosMacros", type: "MakeLensesPeerNonStaticMacro")
//
