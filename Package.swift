// swift-tools-version:5.9
import PackageDescription
import CompilerPluginSupport

let package = Package(
    name: "Ish",
    platforms: [.iOS(.v15), .macOS(.v12)],
    products: [
        .library(
            name: "Ish",
            targets: ["Ish"]
        ),
//        .library(
//            name: "IshMacros",
//            targets: ["IshMacros"]
//        ),
    ],
    dependencies: [
        .package(url: "https://github.com/bow-swift/bow.git", from: "0.8.0"),
        .package(url: "https://github.com/pointfreeco/swift-case-paths", branch: "main"),
        .package(url: "https://github.com/apple/swift-syntax.git", from: "509.0.0")
//        .package(url: "https://github.com/pointfreeco/swift-prelude", branch: "main")
//        .package(url: "https://github.com/apple/swift-collections.git", .upToNextMajor(from: "1.0.0"))
    ],
    targets: [
        .macro(
            name: "IshMacrosMacros",
            dependencies: [
                .product(name: "SwiftSyntaxMacros", package: "swift-syntax"),
                .product(name: "SwiftCompilerPlugin", package: "swift-syntax")
            ]
        ),

        // Library that exposes a macro as part of its API, which is used in client programs.
        //.target(name: "IshMacros", dependencies: ["IshMacrosMacros"]),
        .target(
            name: "Ish",
            dependencies: [
                "IshMacrosMacros",
                .product(name: "Bow", package: "Bow"),
                .product(name: "CasePaths", package: "swift-case-paths"),
//                .product(name: "Prelude", package: "swift-prelude"),
//                .product(name: "Optics", package: "swift-prelude"),
//                .product(name: "Reader", package: "swift-prelude")
//                .product(name: "Collections", package: "swift-collections")
            ])
    ]
)
