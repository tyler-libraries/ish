# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.19.0]
- [update rewrite](changelog/0.19.0/updaterewrite.md)
## [0.16.1]

### Added
- Overloads for scopedActions on the parent to take an index

## [0.16.0]

### Fixed
- Fixed nested subview issue, initialActions will fire now and correctly propogate state changes upwards

### Added
- Added debug logging that is by default off

### Removed
- Removed need for Identifiable States

## [0.15.0]
### Added
- Added back initialAction to subviews, with an internal subviewtracker to properly handle the reconstruction of subviews without infinite recursion of calls in certain cases

## [0.14.0]
### Added
- Feature/FeatureView protocol [See Usage](Examples/FullAps/WidgetIsh/WidgetIsh/)
- subview docs
- [lifecycle diagram](Docs/IshLifecycle.png)

## [0.13.x]
(todo: add info here)

## [0.12.1] - 2023-05-23
- subview changes, added `sourceOfTruth` and `scopedState`
- buildArray added, `IshViewBuilder` generalized to any Array

## [0.11.0] - 2023-05-19

### Added
- Added .% for custom attributes [More Info](changelog/0.11.0/attributes.md)
- Added `SpecificView<S, A, V>` [More Info](changelog/0.11.0/specificview.md)
- New overloads of `fromSwiftUI`/`toSwiftUI` to allow `SpecificView`s to be easily constructed.
- New attributes: `dimBackground`, `aspectRatio`, `spacing`, `vstackAlignment`, `hstackAlignment`, `zstackAlignment`
- New views: `divider`, `spacer`, `group`, `list`

### Changed
- `fromSwiftUI` quick help updated, check it out in xcode!
- Adjusted typealiases around Context to be more clear.
- `subview`s take an optional `initialAction`


## [0.10.0] - 2023-05-17

### Added
- `task` (no dot), encompasses `.task`, `.sideTask`, and what would've been `.throwableTask` [More Info](changelog/0.10.0/task.md)
- direct usage of stage modification lenses [More Info](changelog/0.10.0/lenses.md)
- started added documentation to functions (starting with fromSwiftUI)
- new `alert` implementation as an actual view instead of a side effect, which allows much better integration into the framework

### Deprecated
- `.task` in favor of `task`
- `.sideTask` in favor of `task`
- `.modify` in favor of direct usage of lenses

