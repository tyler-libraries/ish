### Comparing pre-0.10.0 to 0.10.0
This showcases changes to both [task](task.md) and [lenses](lenses.md).

_Given this `Action` type_
```swift
enum Action {
  case increment
  case decrement
  case loadUser
  case setChores([Chore])
  case setChoresError(Error)
}
```

_This is what the code would be like prior to 0.10.0_

```swift
func update(state: State, action: Action) -> [Response<State, Action>] {
  switch action {
  case .increment: 
    return [.sideTask { print("Incremented") }, .modify(\.count +~ 1)]
  case .decrement: 
    return [.sideTask { print("Decremented") }, .modify(\.count -~ 1)]
  case .loadUser:
    return .task {
      guard let user = state.user else { return .setChoresError(MyError("User not found")) }
      do {
        return try await .setChores(user.getChores())
      } catch {
        return .setChoresError(error)
      }
    }
  case .setChores(let chores):
    return .modify(\.chores =~ chores)
  case .setChoresError(let error):
    return .modify(\.errorMessage =~ error.localizedDescription)
  }
}
```

_vs. now_
```swift
func update(state: State, action: Action) -> [Response<State, Action>] {
  switch action {
  case .increment: 
    return [task { print("Incremented") }, \.count +~ 1]
  case .decrement: 
    return [task { print("Decremented") }, \.count -~ 1]
  case .loadUser:
    return task(onThrow: Action.setChoresError) {
      guard let user = state.user else { throw MyError("User not found") }
      return try await .setChores(user.getChores())
    }
  case .setChores(let chores):
    return \.chores =~ chores
  case .setChoresError(let error): 
    return \.errorMessage =~ error.localizedDescription
  }
}
```
