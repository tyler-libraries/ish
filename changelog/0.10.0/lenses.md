## Lens Changes


### Rationale
`.modify` was an incredibly common operation that doesn't add much value readabiity wise over just using the lens directly. Originally the `.modify` case was designed at the same time as `.task` and `.sideTask`, and these were all simply cases on an enum, and the thought was that these lenses might serve a more general purpose than just `modify`. 

The `Model` type in monomer simply takes the data as it should exist, and the idiomatic construction of that is through the use of lenses on the passed in state. Here, there are no lenses that exist outside of the library for generic access/"modification" of simple or nested state, so we don't need the DSL to support the use of some nonexistent generic operators. These were always encoded with KeyPathModifications in mind, and other internal types, so they weren't going to be easily usable outside the context of this framework.

If I wanted to make a more generally accessible lenses library, that would be outside the scope of this project. Embracing the sole usecase of these allows for improved readability and conciseness.

### Comparison
The simplification of the state modification lenses means that:
```swift
.modify(\.count +~ 1)
```
can now be
```swift
\.count +~ 1
```

This applies to all lenses. They still have overloaded variants that'll autowrap them in a list as well.

Here's [an example](comparison.md) using the changes to the state modification lenses.
