## Task Changes

### Rationale
Previously, there existed 2 variants of `.task`, `.task` and `.sideTask`. Both would accept async functions, neither would throw, and the only difference was that the former would return an action that would then be fired by the framework, and the latter wouldn't.

Adding a `throwableTask` would've also required adding a `throwableSideTask`, which would've been inconvenient for end-users. 

So my initial thought was to overload `.task`, trying versions with `onSuccess`, `onFailure`, `onThrow`, and various combinations of them. The issue I ran into was that enums in Swift overloaded by their associated values can't be pattern matched, because the (now deprecated) tuple pattern matching catches the cases that should be handling variants. [This SEP](https://github.com/apple/swift-evolution/blob/master/proposals/0155-normalize-enum-case-representation.md) was supposed to fix this issue, but [it seems](https://forums.swift.org/t/pattern-matching-for-enums-with-same-base-case-and-distinct-associated-values/19368) that either there was a regression between Swift 3 and now, or the issue wasn't fixed as described.

Either way, the path forward was to hide the enum implementation behind a struct+static func pattern, which allows more flexibility in the types (you can overload on associated names as well as types, you can have type arguments specific to a case, etc.), while also maintaining the closed nature by closing off construction of the Response type.

Eventually, I figured why even have them as static functions, I only have 1 overloaded name (task), and since I also wanted to get rid of `.modify`, just allow `task` to be available directly.

### The new `task`
- `task` now covers the cases of `task`, `sideTask`, `throwableTask`, and `throwableSideTask`
- It can return or not return
- If it does return, it has to return an `Action`
- A `task` can only throw if you pass in an `onThrow` function (which can simply be an enum case that aligns with the type `(Error) -> Action`, like in the below example)
- If the task throws, that action will be called with the `Error`, otherwise an action could be returned (or the task could simply run with no return)

Here's [an example](comparison.md) using the new `task`.
