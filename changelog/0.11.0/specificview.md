## SpecificView<S, A, V>

### Rationale
In SwiftUI, `View` is a protocol that all `View`s implement. They all require a body, and these general views paired with the introduction of opaque types to hide away extraneous type information allowed for simple construction and composability of `View`s.

There are a number of reasons why this pattern wouldn't have worked in Ish. 
- `View` isn't a protocol in Ish, it's a `Reader` in `Context` and returning a `SwiftUI.AnyView`, to allow implicit wiring of Context throughout the app.
- The context of action/state is needed for type inference to function properly. Even if we could use some kind of existential qualification or associated types to hide away `State`/`Action`, the inference inside these views wouldn't be able to figure out what action is supposed to be passed to say a `button`.

##### Advantages to Ish approach
- The complexity of wiring the state/actions through the views is hidden from the library user
- It's easy to write abstractions with non-opaque types. For example, repeating the same `View<S, A>` 5 times:
```swift
Array.init(repeating: myIshView, count: 5)
```
This same pattern will work with non-opaque SwiftUI types, but you can't do this with `some View`.

##### Main disadvantage to 0.1 -> 0.10 Ish approach
Working with a specific view type wasn't really possible. If you wanted to require a `Button` be passed somewhere without dropping to SwiftUI, you'd have to create a Struct to wrap the `Ish.View`. However, if you wanted to convert the wrapped `Ish.View` back to a `SwiftUI.View` in the context of a `fromSwiftUI`, you would only get back an `AnyView`.


### Solution
In comes the `SpecificView<S, A, V>`. This allows you to retain type information about the `SwiftUI.View` that is being wrapped. In this release `fromSwiftUI`/`toSwiftUI` were overloaded to return either a `View<S, A>` or `SpecificView<S, A, V>`. The other view functions will be overloaded as well in a later release, either by hand or the upcoming declaration macros in Swift.

Lets look at this type in practice. Say you had an ish view that was wrapping a custom SwiftUI component named `CustomButton`:
```swift
func myCustomButton() -> View<S, A> { // impl }
```

How would you have written a function to require this custom button? If you simply required a `View<S, A>` anything could be passed. The best solution would've been to wrap it in a struct, essentially acting like a Haskell `newtype` wrapper.

```swift
struct MyCustomButton<S, A> {
  let raw: View<S, A>
}
func myCustomButton() -> MyCustomButton<S, A> {
  MyCustomButton(raw: // impl)
}
```

This would work for some cases, but you still don't actually have the `CustomButton` type information, it's been erased, and _technically_ you could construct a `MyCustomButton` with any View, not just a `CustomButton`, which is not ideal.


Here's the solution with `SpecificView<S, A, V>`s:

```swift
func myCustomButton() -> SpecificView<S, A, CustomButton> {
    // impl
}
```

And this type can still be easily composed with `View<S, A>`! All you have to do is call `myCustomButton().toView()` and you have a `View`.

Now if you try to use `SpecificView` with standard SwiftUI components, you'll find type signatures get a bit more annoying:

```swift
func mySwiftUIButton<V: View>() -> SpecificView<S, A, Button<V>> {
 // impl
}
```

Eventually, with some new `typealias`es and simulating type-parameter currying, this might get nicer.
