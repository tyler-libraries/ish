## Custom Attribute operator

### Rationale
Previously, the only way to implement custom attributes idiomatically was to extend `Attribute` with public static functions, this is still the best way to do it for attributes that are reused a lot, but this change makes it easy to inline a new attribute

### The new operator
Here's a snippet of SwiftUI code
```swift
List {
    // content
}
.listStyle(.plain)

```

now without the need for a library update or a custom public static function extension for specifically this view modifier, you can use it as an attribute in an Ish view like so:

```swift
list(attributes: [
  .%{ $0.listStyle(.plain) }
])
```

There are also 2 named aliases available for this, one of these will be deprecated in favor of the other eventually:
```swift
list(attributes: [
  .custom { $0.listStyle(.plain) }
])
```
```swift
list(attributes: [
  .attribute { $0.listStyle(.plain) }
])
```

### Considerations
There are some view modifiers that exist outside the scope of what attributes in this framework should do, like the `.task` modifier. Take care to avoid these in favor of more idiomatic Ish approaches. For example, if you had this SwiftUI code:

```swift
VStack {
  // Content
}.task {
  await viewModel.fetchData()
}
```

You don't want to write this as a custom attribute. Instead, you want to invoke this as an `initialAction` either on the `startLoop` or `subview` function, like so:
```swift
startLoop(
  // other parameters,
  initialAction: .fetchData
)
```

**Attributes should not execute arbitrary side effects, they should be part of the pure description of the view**
