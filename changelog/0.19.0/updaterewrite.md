### Update Rewrite


Updates are now independent of the view. Before this, `asSubview` would actually *create* the subcontext that the subviews existed in, which means the entire state/action/update lifecycle was implicitly tied to subviews connecting them together. This is the magic that was allowing subupdates to implicitly call upwards to parent updates, or allowing `childTask`s to explicitly call downwards to subupdates. This had unintended consequences for sequencing state modifications across these context boundaries.

Now, there is only one update, the one called by `startLoop` (when calling `startLoop` on a feature, it's said feature's update). The "subupdates" are simply other state/action/update loops that are called in the main update and scoped to it (e.g subactions are wrapped in the correct case). This is to prevent children from having to say `.topLevelCase(.midLevelCase(.thisActionCase)))` and the same with state. So they seem like they're independent sublifecycles, but they're just being scoped into the one actual lifecycle. There is no more implicit calling of subupdates (there'd be no where to get this implicit context), and no more explicit calling of subupdates (as there's technically no such thing), so `childTask` also doesn't exist.

As a result of Views not managing subcontexts, they no longer need `<S, A, PS, PA, SS, AA>` (appState, appAction, parentState, parentAction, selfState, selfAction). Now they only need `A`, the action context the view uses (for things like `button(action: .someAction ...)`. `ViewF` is a typealias for `Ish.View<Action>` inside a FeatureView.

In practice, although there is now only one update, there is the appearance of parent/child updates. The relationship here will always be explicit. Imagine the following:

```python
# before
grandchildView has a button that emits SomeAction
  -> grandchildUpdate handles SomeAction, implicitly calls childUpdate, scoping to ChildAction.grandchildAction
  -> childUpdate handles it, implicitly calls parentUpdate, scoping to Action.childAction
  -> update handles it
```

now, the scoping is handled in both the update and view separately, the view scopes the action **then** calls the update
```python
# after
grandchildView has a button that emits SomeAction, scoping to Action.childAction.grandchildAction
  -> update handles it, explicitly calls childUpdate
  -> childUpdate handles it, explicitly calls grandchildUpdate
  -> grandchildUpdate handles it
```

The cool thing about the explicit nature of the new version is that it's also explicit about ordering, they're just function calls. If you write:

```swift
Child.update(...)
otherUpdateCode
```
this will call the child update first then the parent update, but doing this:
```swift
otherUpdateCode
Child.update(...)
```
will do the opposite.

The original "before" was only in theory too, it wasn't fully functional. Since subviews **created** subcontexts and managed them, when an update emited a state update, it would generate a new subcontext. This wouldn't cause an infinite loop because state "modifications" were descriptions instead of imperative actions, but it did mean that if you did a state modification to generate children, and then called childTask on the new children, it wouldn't work because the original context didn't have reference to the update handlers for the new children contexts.
