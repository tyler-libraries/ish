//import ComposableArchitecture
//import SwiftUI
//
//infix operator +~: AdditionPrecedence
//
//struct SomeValues {
//  let age: Int
//}
//
//func fn(_ xs: SomeValues) -> SomeValues {
//  SomeValues(age: xs.age + 1)
//}
//
//struct Todo: ReducerProtocol {
//  struct State: Equatable, Identifiable {
//    var description = ""
//    let id: UUID
//    var isComplete = false
//  }
//
//  enum Action: Equatable {
//    case checkBoxToggled
//    case textFieldChanged(String)
//  }
//
//  func reduce(into state: inout State, action: Action) -> EffectTask<Action> {
//    switch action {
//    case .checkBoxToggled:
//      state.isComplete.toggle()
//      return .none
//
//    case let .textFieldChanged(description):
//      state.description = description
//      return .none
//    }
//  }
//}
//
//
//let compV: PartialComposableView<Feature1> = PartialComposableView()
//
////let featureView = PartialComposableView<Feature1>()({ viewStore in
////  VStack {
////    HStack {
////      Button("−") { viewStore.send(.decrementButtonTapped) }
////      Text("\(viewStore.count)")
////      Button("+") { viewStore.send(.incrementButtonTapped) }
////    }
////
////    Button("Number fact") { viewStore.send(.numberFactButtonTapped) }
////  }
////  .alert(
////    item: viewStore.binding(
////      get: { $0.numberFactAlert.map(FactAlert.init(title:)) },
////      send: .factAlertDismissed
////    ),
////    content: { SwiftUI.Alert(title: Text($0.title)) }
////  )
////})
//
//
//typealias MkComposableView<T: ReducerProtocol, V: View> = ((Store<T.State, T.Action>) -> ComposableView<T, V>) where T.State: Equatable
//func mkView<T: ReducerProtocol, V: View>(_ fn: @escaping (ViewStore<T.State, T.Action>) -> V) -> ((Store<T.State, T.Action>) -> ComposableView<T, V>) where T.State: Equatable {
//  return { store in return ComposableView(store: store, fn: fn) }
//}
//
//struct PartialComposableView<T: ReducerProtocol> where T.State: Equatable {
//  func callAsFunction<V: View>(_ fn: @escaping (ViewStore<T.State, T.Action>) -> V) -> ((Store<T.State, T.Action>) -> ComposableView<T, V>) where T.State: Equatable {
//    return { store in ComposableView<T, V>(store: store, fn: fn) }
//  }
//}
//
//struct ComposableView<T: ReducerProtocol, V: View>: View where T.State: Equatable {
//  let store: StoreOf<T>
//  let fn: (ViewStore<T.State, T.Action>) -> V
//  var body: WithViewStore<T.State, T.Action, V> {
//    WithViewStore(self.store, observe: { $0 }) { viewStore in
//      fn(viewStore)
//    }
//  }
//}
//struct NewTodoView: ComposableView2 {
//  typealias StoreType = Todo
//  
//  
//  var store: StoreOf<Todo>
//
//  func mkView(_ viewStore: ViewStore<Todo.State, Todo.Action>) -> some View {
//    return fatalError()
//  }
//}
//
//struct OldTodoView: View {
//    let store: StoreOf<Todo>
//    
//    var body: some View {
//        WithViewStore(self.store, observe: { $0 }) { viewStore in
//            fatalError()
//        }
//    }
//}
//
//
//struct TodoView: View {
//  let store: StoreOf<Todo>
//
//  var body: some View {
//    WithViewStore(self.store, observe: { $0 }) { viewStore in
//      HStack {
//        Button(action: { viewStore.send(.checkBoxToggled) }) {
//          Image(systemName: viewStore.isComplete ? "checkmark.square" : "square")
//        }
//        .buttonStyle(.plain)
//
//        TextField(
//          "Untitled Todo",
//          text: viewStore.binding(get: \.description, send: Todo.Action.textFieldChanged)
//        )
//      }
//      .foregroundColor(viewStore.isComplete ? .gray : nil)
//    }
//  }
//}
//
//// IDEALLY DEFINED IN LIBRARY
//protocol ComposableView2: View {
//    associatedtype StoreType: ReducerProtocol where StoreType.State: Equatable
//    associatedtype SomeView: View
//    var store: StoreOf<StoreType> { get }
//    
//    func mkView(_ viewStore: ViewStore<StoreType.State, StoreType.Action>) -> SomeView
//}
//
//extension ComposableView2 {
//    var body: some View {
//        WithViewStore(self.store, observe: { $0 }) { viewStore in
//            mkView(viewStore)
//        }
//    }
//}
