//import ComposableArchitecture
import SwiftUI
//
//struct User {
//  let name: String
//  let age: Int
//}
//
//
struct MyTodosApp: App {
  var body: some Scene {
    WindowGroup {
//      Feature1View(
//        store: Store(
//          initialState: Feature.State(),
//          reducer: Feature()
//        )
//      )
    }
  }
}
//
//struct NumberFactFeature: ReducerProtocol {
//  struct State: Equatable {
//    var count = 0
//    var numberFactAlert: String?
//  }
//  enum Action: Equatable {
//    case factAlertDismissed
//    case decrementButtonTapped
//    case incrementButtonTapped
//    case numberFactButtonTapped
//    case numberFactResponse(String?)
//  }
//
//  func reduce(into state: inout State, action: Action) -> EffectTask<Action> {
//    switch action {
//      case .factAlertDismissed:
//        state.numberFactAlert = nil
//        return .none
//
//      case .decrementButtonTapped:
//        state.count -= 1
//        return .none
//
//      case .incrementButtonTapped:
//        state.count += 1
//        return .none
//
//      case .numberFactButtonTapped:
//        return .task { [count = state.count] in
//          let url = "http://numbersapi.com/\(count)/trivia"
//          return .numberFactResponse(try? await String(
//            decoding: URLSession.shared.data(from: URL(string:url)!).0,
//            as: UTF8.self
//          ))
//        }
//
//      case .numberFactResponse(nil):
//      state.numberFactAlert = "Could not load a number fact :("
//        return .none
//
//      case let .numberFactResponse(fact):
//        state.numberFactAlert = fact
//        return .none
//    }
//  }
//}
//
//struct NumberFactView: View {
//  let store: StoreOf<NumberFactFeature>
//
//  var body: some View {
//    WithViewStore(self.store, observe: { $0 }, content: { viewStore in
//      VStack {
//        HStack {
//          Button("−") { viewStore.send(.decrementButtonTapped) }
//          Text("\(viewStore.count)")
//          Button("+") { viewStore.send(.incrementButtonTapped) }
//        }
//
//        Button("Number fact") { viewStore.send(.numberFactButtonTapped) }
//      }
//      .alert(
//        item: viewStore.binding(
//          get: { $0.numberFactAlert.map(FactAlert.init(title:)) },
//          send: .factAlertDismissed
//        ),
//        content: { Alert(title: Text($0.title)) }
//      )
//    }
//  }
//}
//
//
//struct FactAlert: Identifiable {
//  var title: String
//  var id: String { self.title }
//}
//
//
//// MY COMPOSABLE
////import ComposableArchitecture
////import SwiftUI
////
////struct Feature: ReducerProtocol {
////  struct State: Equatable {
////    var count = 0
////    var numberFactAlert: String?
////  }
////
////  enum Action: Equatable {
////    case factAlertDismissed
////    case decrementButtonTapped
////    case incrementButtonTapped
////    case numberFactButtonTapped
////    case numberFactResponse(TaskResult<String>)
////  }
////
////  func reduce(into state: inout State, action: Action) -> EffectTask<Action> {
////    func modify(_ fn: () -> Unit) -> EffectTask<Action> {
////      fn()
////      return .none
////    }
////    return switch action {
////      case .factAlertDismissed: modify { state.numberFactAlert = nil }
////      case .decrementButtonTapped: modify { state.count -= 1 }
////      case .incrementButtonTapped: modify { state.count += 1 }
////      case .numberFactButtonTapped:
////        func url(_ count: Int) {
////          return "http://numbersapi.com/\(count)/trivia"
////        }
////        .task { [count = state.count] in
////          await .numberFactResponse(
////            TaskResult {
////              String(
////                decoding: try await URLSession
////                  .shared
////                  .data(from: URL(string: url(count))!)
////                  .0,
////                as: UTF8.self
////              )
////            }
////          )
////        }
////      case let .numberFactResponse(.success(fact)):
////        modify { state.numberFactAlert = fact }
////      case .numberFactResponse(.failure):
////        modify { state.numberFactAlert = "Could not load a number fact :(" }
////    }
////  }
////}
////
////
////                  func buildUI(state: State) {
////
////    }
////// client code
////let featureView: MkComposableView<Feature> = composeViewWithState { state in
////  if let text = state.alertText {
////    Alert(display: text, onDismiss: .factAlertDismissed)
////  } else {
////    VStack {
////      HStack {
////        Button("−", .decrementButtonTapped)
////        Text("\(viewStore.count)")
////        Button("+", .incrementButtonTapped)
////      }
////      Button("Number fact", .numberFactButtonTapped)
////    }
////  }
////}
////
////@main
////struct MyApp: App {
////  var body: some Scene {
////    WindowGroup { featureView(Feature.State(), Feature()) }
////  }
////}
