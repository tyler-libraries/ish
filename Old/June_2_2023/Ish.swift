import SwiftUI
import Bow
import Foundation
import Combine

public typealias UUID = Foundation.UUID

//public func startLoop<S, A>(
//    initialState: S,
//    buildUI: @escaping (S) -> View<S, A>,
//    update: @escaping (S, A) -> [Response<S, A>] = emptyUpdate,
//    initialAction: A? = nil
//) -> some SwiftUIView {
//    createContextAndConsumer(
//        state: initialState,
//        stateConsumer: { state, _ in buildUI(state) },
//        update: update,
//        handleInitialAction: { context in
//            if let initialAction {
//                handleEvent(context: context, action: initialAction, publishChanges: true)
//            }
//        }
//    )
//}
//
//public func startLoop<A>(
//    buildUI: @escaping () -> View<(), A>
//) -> some SwiftUIView {
//    createContextAndConsumer(
//        state: (),
//        stateConsumer: { _, _ in buildUI() },
//        update: emptyUpdate,
//        handleInitialAction: { _ in }
//    )
//}

func createContextAndConsumer<S, A>(
    state: S,
    stateConsumer: @escaping (S, Context<S, A>) -> View<S, A>,
    update: @escaping (S, A) -> [Response<S, A>],
    handleInitialAction: (Context<S, A>) -> (),
    parentAction: @escaping (A) -> () = { _ in }
) -> some SwiftUIView {
    let context: Context<S, A> = Context(
        state: state,
        parentActionRun: parentAction,
        parentStateModify: { _ in true },
        update: { (s, a) in
            update(s,a).map { $0.internalResponse }
        }
    )
    let contextConsumer: ContextConsumer<S, A> = ContextConsumer(
        context: context,
        stateConsumer: {
            IshDebug.log("#createContextAndConsumer: for \($0)")
            return stateConsumer($0, context)
        }
    )
    
    handleInitialAction(context)
    
    // context.publishChange()
    
    return contextConsumer
}


class SubviewTracker {
    let id: UUID
    var previouslyEnabled: Bool
    
    init(_ id: UUID) {
        self.id = id
        self.previouslyEnabled = false
    }
}

public struct IshDebug {
    public static var logging: Bool = false
    public static var loggingMechanism: (String) -> () = { message in
        if logging {
            print(message)
        }
    }
    
    static var log = loggingMechanism
}

struct SharedContext {
    static var statesToUpdate: [() -> ()] = []
    static var subviewTrackers: [SubviewTracker] = []
    static var currentSubview: Int = 0
}

public class Context<S, A>: ObservableObject {
    var state: (S)
    var scopeRun = false
    var scopeStateRun = false
    var potentiallyScopedAction: A? = nil
    var parentActionRun: (A) -> ()
    var parentStateModify: (S) -> (Bool)
    let update: (S, A) -> [InternalResponse<S, A>]
    var subscriptions = Set<AnyCancellable>()
    
    init(
        state: S,
        parentActionRun: @escaping (A) -> () = { _ in },
        parentStateModify: @escaping (S) -> Bool,
        update: @escaping (S, A) -> [InternalResponse<S, A>]
    ) {
        self.state = state
        self.parentActionRun = parentActionRun
        self.parentStateModify = parentStateModify
        self.update = update
    }
        
    func runParentActionIfExists() {
        IshDebug.log("#runParentActionIfExists: for \(state)")
        if let potentiallyScopedAction {
            IshDebug.log("#runParentActionIfExists: scoped action exists")
            parentActionRun(potentiallyScopedAction)
            self.potentiallyScopedAction = nil
        }
    }
    
    func modifyCurrentAndParentState(modified: S) {
        self.state = modified
        // TODO: use enum for clarity
        if self.parentStateModify(modified) {
            /// NOTE: Scoped state run shouldn't be necessary
            /// without publishing changes on parents the view shouldn't rerender until it needs to, and the state will be correct at that point anyway. Subviews will contain the correct view representation during the interim
            StateQueue.asyncMain {
                if self.scopeStateRun == false {
                    self.scopeStateRun = true
                    self.publishChange()
                } else {
                    // self.scopeStateRun = false
                }
            }
        }
    }
    
    @MainActor
    func modifyState(_ newState: S) async {
        self.state = newState
    }
    
    func publishChange() {
        self.objectWillChange.send()
    }
    
//    func subscribeToSubview<SS, AA>(
//        subcontext: Context<SS, AA>,
//        modification: @escaping (S, SS) -> S,
//        scopeRun: Bool
////        scopedState: WritableKeyPath<S, SS>? = nil,
////        scopedStateNillable: WritableKeyPath<S, SS?>? = nil
//    ) where S: Equatable {
//        subcontext.$state.sink { [weak self] substate in
//            guard let self else { return }
//            DispatchQueue.main.async {
//                IshDebug.log("\n#subscribeToSubview.sink: state - \(self.state)")
//                IshDebug.log("#subscribeToSubview.sink: substate - \(substate)")
//                IshDebug.log("#subscribeToSubview.sink: modified - \(modification(self.state, substate))")
//                if scopeRun == false {
//                    self.scopeRun = true
//                    let modified = modification(self.state, substate)
//                    if self.state != modified {
//                        self.state = modified
//                        // self.objectWillChange.send()
//                    }
//                } else {
//                    // self.scopeRun = false
//                }
//            }
////            if let scopedState {
////                self.state = (scopedState =~ substate)(self.state)
////            }
////
////            if let scopedStateNillable {
////                self.state = (scopedStateNillable =~ substate)(self.state)
////            }
//        }.store(in: &subscriptions)
//
//        // subscriptions.append(sub)
//        IshDebug.log("#subscribeToSubview: count - \(subscriptions.count)")
////        print(subscriptions.count)
//    }
}

struct ContextConsumer<S, A>: SwiftUIView {
    @ObservedObject var context: Context<S, A>
    let stateConsumer: (S) -> View<S, A>
    
    var body: some SwiftUIView {
        stateConsumer(context.state).toSwiftUI(context)
    }
}

struct StateQueue {
    private static let queue = DispatchQueue(label: "com.Ish.StateQueue")
    
    static func `asyncMain`(_ fn: @escaping () -> ()) {
        queue.async {
            DispatchQueue.main.sync {
                fn()
            }
        }
    }
    
    static func `async`(_ fn: @escaping () -> ()) {
        queue.async {
            fn()
        }
    }
}

func handleEvent<S, A>(
    context: Context<S, A>,
    action: A,
    publishChanges: Bool = true
    // parentStateModify: @escaping (S) -> () = { _ in }
//    parentStateModify: @escaping (S) -> () = { _ in }
) {
    
    let responses = context.update(context.state, action)
    let modifications = responses.modifications()
    
//    Task {
//        for m in modifications.dropLast(1) {
//            IshDebug.log("\n#handleEvent: main-loop, unmodified: \(context.state)")
//            IshDebug.log("#handleEvent: main-loop, modified: \(m(context.state))")
//            await context.modifyState(m(context.state))
//        }
//
//        if let m = modifications.last {
//             IshDebug.log("\n#handleEvent: last-item, unmodified: \(context.state)")
//            IshDebug.log("#handleEvent: last-item, modified: \(m(context.state))")
//            context.potentiallyScopedAction = action
//            await context.modifyState(m(context.state))
//        }
//    }
    for m in modifications.dropLast(1) {

        // let modified = m(context.state)
        StateQueue.asyncMain {
            IshDebug.log("\n#handleEvent: main-loop, unmodified: \(context.state)")
            IshDebug.log("#handleEvent: main-loop, modified: \(m(context.state))")
            
            context.modifyCurrentAndParentState(modified: m(context.state))
                       //            parentStateModify(m(context.state))
        }
    }
    if let m = modifications.last {
        StateQueue.asyncMain {
            IshDebug.log("\n#handleEvent: last-item, unmodified: \(context.state)")
            IshDebug.log("#handleEvent: last-item, modified: \(m(context.state))")
            // context.potentiallyScopedAction = action
            context.modifyCurrentAndParentState(modified: m(context.state))
            //            parentStateModify(m(context.state))
        }
    } else {
//        StateQueue.async {
//            context.potentiallyScopedAction = action
//            context.runParentActionIfExists()
//        }
    }
    
    context.parentActionRun(action)
    
    StateQueue.asyncMain {
        IshDebug.log("#handleEvent: publishing changes")
        if publishChanges {
            context.publishChange()
        }
//        IshDebug.log("#handleEvent: parent-state-modify AFTER last-item")
//         parentStateModify(context.state)
        // context.objectWillChange.send()
        
    }
    
    StateQueue.asyncMain {
        IshDebug.log("#handleEvent: starting handleTasks")
        handleTasks(context: context, responses: responses, publishChanges: publishChanges)
    }
}

func handleTasks<S, A>(context: Context<S, A>, responses: [InternalResponse<S, A>], publishChanges: Bool) {
    for response in responses {
        switch response {
        case let .throwableSideTask(onThrow, io):
            StateQueue.async {
                Task {
                    do {
                        // try await Task.sleep(nanoseconds: 5_000_000_000)
                        try await io()
                    } catch {
                        handleEvent(context: context, action: onThrow(error), publishChanges: publishChanges)
                    }
                }
            }
            
        case let .throwableTask(onThrow, io):
            StateQueue.async {
                Task {
                    var result: A
                    do {
                        // try await Task.sleep(nanoseconds: 5_000_000_000)
                        result = try await io()
                    } catch {
                        result = onThrow(error)
                    }
                    handleEvent(context: context, action: result, publishChanges: publishChanges)
                }
            }
            
        case .task(fn: let io):
            StateQueue.async {
                Task {
                    // context.publishChange()
                    // try await Task.sleep(nanoseconds: 5_000_000_000)
                    let nextAction = await io()
                    handleEvent(context: context, action: nextAction, publishChanges: publishChanges)
                }
            }
            
        case let .sideTask(io):
            StateQueue.async {
                Task.detached { await io () }
            }
        case .modify(_): ()
        }
    }
}
