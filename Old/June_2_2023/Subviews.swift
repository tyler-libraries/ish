import Foundation

/// Build a childViews ([View<SS,AA>]) as parentViews ([View<S,A>]). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subviews(
///     states: parentState.childStates,
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubviews(states: parentState.childStates)
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStatesOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists singular versions of the above as well (`subview` & `asSubview`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - states: The list of child states 1:1 with the resulting list of views
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given list of child states when a child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A list of parentViews that encapsulate the lifecycles of the child views
public func subviews<S: Equatable, A, SS: Equatable, AA>(
    states: [SS],
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, [SS]?>,
    scopedAction: @escaping (Int, AA) -> A? = { _, _ in nil },
    initialAction: AA? = nil
) -> [View<S, A>] {
    states.enumerated().map { (i, s) in
        subviewImpl(
            s,
            buildUI,
            update,
            { scopedAction(i, $0) },
            initialAction,
            modification: { oldParentState, childState in
                let oldChildState = oldParentState[keyPath: scopedState]
                
                if let oldChildState {
                    let m = scopedState =~ oldChildState.replaceAtIndex(i, childState)
                    return m(oldParentState)
                } else {
                    return oldParentState
                }
                
//                let m = scopedState =~ states.replaceAtIndex(i, childState)
//                return m(oldParentState)
            }
        )
//        ) { state, pc, c in
//            modifyIfChanged(scopedState =~ states.replaceAtIndex(i, state), pc, c)
//        }
    }
}

/// Build a childViews ([View<SS,AA>]) as parentViews ([View<S,A>]). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subviews(
///     states: parentState.childStates,
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubviews(states: parentState.childStates)
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStatesOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists singular versions of the above as well (`subview` & `asSubview`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - states: The list of child states 1:1 with the resulting list of views
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given list of child states when a child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A list of parentViews that encapsulate the lifecycles of the child views
public func subviews<S: Equatable, A, SS: Equatable, AA>(
    states: [SS],
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, [SS]?>,
    scopedAction: @escaping (AA) -> A? = { _ in nil },
    initialAction: AA? = nil
) -> [View<S, A>] {
    states.enumerated().map { (i, s) in
        subviewImpl(
            s,
            buildUI,
            update,
            scopedAction,
            initialAction,
            modification: { oldParentState, childState in
                let oldChildState = oldParentState[keyPath: scopedState]
                
                if let oldChildState {
                    let m = scopedState =~ oldChildState.replaceAtIndex(i, childState)
                    return m(oldParentState)
                } else {
                    return oldParentState
                }
               
//                let m = scopedState =~ states.replaceAtIndex(i, childState)
//                return m(oldParentState)
            }
        )
//        ) { state, pc, c in
//            modifyIfChanged(scopedState =~ states.replaceAtIndex(i, state), pc, c)
//        }
    }
}

/// Build a childViews ([View<SS,AA>]) as parentViews ([View<S,A>]). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subviews(
///     states: parentState.childStates,
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubviews(states: parentState.childStates)
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStatesOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists singular versions of the above as well (`subview` & `asSubview`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - states: The list of child states 1:1 with the resulting list of views
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given list of child states when a child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A list of parentViews that encapsulate the lifecycles of the child views
public func subviews<S: Equatable, A, SS: Equatable, AA>(
    states: [SS],
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, [SS]>? = nil,
    scopedAction: @escaping (Int, AA) -> A? = { _, _ in nil },
    initialAction: AA? = nil
) -> [View<S, A>] {
    states.enumerated().map { (i, s) in
        subviewImpl(
            s,
            buildUI,
            update,
            { scopedAction(i, $0) },
            initialAction,
            modification: { oldParentState, childState in
                if let scopedState {
                    let oldChildState = oldParentState[keyPath: scopedState]
                    let m = scopedState =~ oldChildState.replaceAtIndex(i, childState)
                    return m(oldParentState)
                } else {
                    return oldParentState
                }
//                scopedState.map {
//                    let m = ($0 =~ states.replaceAtIndex(i, childState))
//                    return m(oldParentState)
//                } ?? oldParentState
            }
//            scopedState: scopedState.map {
//                let m = ($0 =~ states.replaceAtIndex(i, s))
//                return m(
//            }
        )
//        { state, pc, c in
//            if let scopedState {
//                modifyIfChanged(scopedState =~ states.replaceAtIndex(i, state), pc, c)
//            } else {
//                c.runParentActionIfExists()
//            }
//        }
    }
}

/// Build a childViews ([View<SS,AA>]) as parentViews ([View<S,A>]). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subviews(
///     states: parentState.childStates,
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubviews(states: parentState.childStates)
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStatesOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists singular versions of the above as well (`subview` & `asSubview`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - states: The list of child states 1:1 with the resulting list of views
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given list of child states when a child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A list of parentViews that encapsulate the lifecycles of the child views
public func subviews<S: Equatable, A, SS: Equatable, AA>(
    states: [SS],
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, [SS]>? = nil,
    scopedAction: @escaping (AA) -> A? = { _ in nil },
    initialAction: AA? = nil
) -> [View<S, A>] {
    states.enumerated().map { (i, s) in
        subviewImpl(
            s,
            buildUI,
            update,
            scopedAction,
            initialAction,
            modification: { oldParentState, childState in
                if let scopedState {
                    let oldChildState =
                        oldParentState[keyPath: scopedState]
                    let m = scopedState =~ oldChildState.replaceAtIndex(i, childState)
                    return m(oldParentState)
                } else {
                    return oldParentState
                }
//                scopedState.map {
//                    let m = ($0 =~ states.replaceAtIndex(i, childState))
//                    return m(oldParentState)
//                } ?? oldParentState
            }
        )
//            scopedState: scopedState.map {
//                let m = ($0 =~ states.replaceAtIndex(i, s))
//                return m(
//            }
//        ) { state, pc, c in
//            if let scopedState {
//                modifyIfChanged(scopedState =~ states.replaceAtIndex(i, state), pc, c)
//            } else {
//                c.runParentActionIfExists()
//            }
//        }
    }
}
/// Build a childView (View<SS,AA>) as a parentView (View<S,A>). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subview(
///     state: Child.State(),
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubview(state: Child.State())
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStateOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists the plural version of the above as well (`subviews` & `asSubviews`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - state: The child state for the new subview
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given child state when the child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A parentView that encapsulates the lifecycle of a child view
public func subview<S: Equatable, A, SS: Equatable, AA>(
    state: SS,
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, SS?>,
    scopedAction: @escaping (AA) -> A? = { _ in nil },
    initialAction: AA? = nil
) -> View<S, A> {
    subview(
        initialState: state,
        buildUI: buildUI,
        update: update,
        scopedState: scopedState,
        scopedAction: scopedAction,
        initialAction: initialAction
    )
}

/// Build a childView (View<SS,AA>) as a parentView (View<S,A>). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subview(
///     state: Child.State(),
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubview(state: Child.State())
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStateOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists the plural version of the above as well (`subviews` & `asSubviews`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - state: The child state for the new subview
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given child state when the child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A parentView that encapsulates the lifecycle of a child view
public func subview<S: Equatable, A, SS: Equatable, AA>(
    state: SS,
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, SS>? = nil,
    scopedAction: @escaping (AA) -> A? = { _ in nil },
    initialAction: AA? = nil
) -> View<S, A> {
    subview(
        initialState: state,
        buildUI: buildUI,
        update: update,
        scopedState: scopedState,
        scopedAction: scopedAction,
        initialAction: initialAction
    )
}

/// Build a childView (View<SS,AA>) as a parentView (View<S,A>). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subview(
///     state: Child.State(),
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubview(state: Child.State())
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStateOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists the plural version of the above as well (`subviews` & `asSubviews`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - state: The child state for the new subview
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given child state when the child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A parentView that encapsulates the lifecycle of a child view
@available(*, deprecated, message: "\nUse `state:` instead of `initialState:`")
public func subview<S: Equatable, A, SS: Equatable, AA>(
    initialState state: SS,
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, SS?>,
    scopedAction: @escaping (AA) -> A? = { _ in nil },
    initialAction: AA? = nil
) -> View<S, A> {
    subviewImpl(
        state,
        buildUI,
        update,
        scopedAction,
        initialAction,
        modification: { oldParentState, childState in
            (scopedState =~ childState)(oldParentState)
        }
    )
        // scopedStateNillable: scopedState
//    ) { state, parentContext, c in
//        modifyIfChanged(scopedState =~ state, parentContext, c)
//    }
}

/// Build a childView (View<SS,AA>) as a parentView (View<S,A>). `Show Quick Help` for more information
///
/// A simple usage of this function, very similar to `startLoop`:
/// ```
/// subview(
///     state: Child.State(),
///     buildUI: Child.buildUI,
///     update: Child.update
/// )
/// ```
///
/// If you decide to use `Feature`/`FeatureView` protocols to describe your features, you can also do this:
/// ```
/// Child.asSubview(state: Child.State())
/// ```
///
/// Commonly, you'll want to attach subview state/actions to parent view state/actions. You can optionally pass `scopedState` and `scopedAction` (scroll down for descriptions):
/// ```
/// scopedState: \.childStateOnParent
/// scopedAction: { childActionOnParent($0) }
/// ```
/// There exists the plural version of the above as well (`subviews` & `asSubviews`).
///
/// If you're interested in how child/parent lifecycles interact, including scopedState/scopedAction, here's a [lifecycle diagram](https://gitlab.com/tyler-libraries/ish/-/tree/main/Docs/IshLifecycle.png)
///
/// - Parameters:
///     - state: The child state for the new subview
///     - buildUI: A function that returns the child view given a child state
///     - update: A function that returns a list of Responses given a child state and child action
///     - scopedState: A WritableKeyPath used to "modify" (construct a new immutable) parent state with a given child state when the child state is changed.
///     - scopedAction: A function that wraps a subaction in a parent action, so the parent's `update` function can be called when the child `update` finishes.
/// - Returns: A parentView that encapsulates the lifecycle of a child view
@available(*, deprecated, message: "\nUse `state:` instead of `initialState:`")
public func subview<S: Equatable, A, SS: Equatable, AA>(
    initialState state: SS,
    buildUI: @escaping (SS) -> View<SS, AA>,
    update: @escaping (SS, AA) -> [Response<SS, AA>],
    scopedState: WritableKeyPath<S, SS>? = nil,
    scopedAction: @escaping (AA) -> A? = { _ in nil },
    initialAction: AA? = nil
) -> View<S, A> {
    subviewImpl(
        state,
        buildUI,
        update,
        scopedAction,
        initialAction,
        modification: { oldParentState, childState in
            scopedState.map { ($0 =~ childState)(oldParentState) }
                ?? oldParentState
        }
    )
        // scopedState: scopedState
//    ) { state, parentContext, c in
//        if let scopedState {
//            modifyIfChanged(scopedState =~ state, parentContext, c)
//        } else {
//            c.runParentActionIfExists()
//        }
//    }
}
private func subviewImpl<S: Equatable, A, SS: Equatable, AA>(
    _ state: SS,
    _ buildUI: @escaping (SS) -> View<SS, AA>,
    _ update: @escaping (SS, AA) -> [Response<SS, AA>],
    _ scopedAction: @escaping (AA) -> A? = { _ in nil },
    _ initialChildAction: AA? = nil,
    modification: @escaping (S, SS) -> S
//    scopedState: WritableKeyPath<S, SS>? = nil,
//    scopedStateNillable: WritableKeyPath<S, SS?>? = nil,
//    _ stateConsumption: @escaping (SS, Context<S, A>, Context<SS, AA>) -> ()
) -> View<S, A> {
//    let subviewTracker = SharedContext.subviewTrackers.first { $0.id == state.id } ?? {
//        let tracker = SubviewTracker(state.id)
//        SharedContext.subviewTrackers.append(tracker)
//        return tracker
//    }()
    return .withContext { parentContext in
        IshDebug.log("\n#subviewImpl: creation for - \(state)")
        let childContext = Context(
            state: state,
            parentActionRun: { aa in
                if let parentAction = scopedAction(aa) {
                    parentContext.scopeRun = true
                    handleEvent(
                        context: parentContext,
                        action: parentAction,
                        publishChanges: true
                    )
                }
            },
            parentStateModify: { ss in
                parentContext.modifyCurrentAndParentState(
                    modified: modification(parentContext.state, ss)
                )
                return false
            }
        ) { (s, a) in
            update(s,a).map { $0.internalResponse }
        }
        
//        parentContext.subscribeToSubview(
//            subcontext: childContext,
//            modification: modification,
//            scopeRun: parentContext.scopeRun
//        )

        // qsleep(1)
        let contextConsumer: ContextConsumer<SS, AA> = ContextConsumer(
            context: childContext,
            stateConsumer: { childState in
                IshDebug.log("#subviewImpl: stateConsumer")
                StateQueue.asyncMain {
                    parentContext.modifyCurrentAndParentState(
                        modified: modification(parentContext.state, childState)
                    )
                    // childContext.runParentActionIfExists()
                }
//                stateConsumption(childState, parentContext, childContext)
//                if childState != childContext.state {
//                    parentContext.objectWillChange.send()
//                }
                return buildUI(childState)
            })
        
        if let action = initialChildAction {
            if parentContext.scopeRun == false {
                IshDebug.log("#subviewImpl: initialChildAction")
                handleEvent(context: childContext, action: action, publishChanges: true)
            } else {
                parentContext.scopeRun = false
            }
//            handleEvent(
//                context: childContext,
//                action: initialChildAction
////                parentStateModify: { childState in
////                    stateConsumption(childState, parentContext, childContext)
////                }
//            )
        }

        IshDebug.log("#subviewImpl: finished")
        return contextConsumer
        
//        createContextAndConsumer(
//            state: state,
//            stateConsumer: { childState, childContext in
//                stateConsumption(childState, parentContext, childContext)
//                return buildUI(childState).applyAttributes([
//                    .attribute { $0.onAppear {
//                        if let initialChildAction {
//                            parentContext.subscribeToSubview(
//                                subcontext: childContext,
//                                modification: modification
////                                scopedState: scopedState,
////                                scopedStateNillable: scopedStateNillable
//                            )
//                            handleEvent(
//                                context: childContext,
//                                action: initialChildAction
////                                parentStateModify: { childState in
////                                    parentContext.state = modification(
////                                        parentContext.state,
////                                        childState
////                                    )
////                                    // parentContext.state = (scopedState =~ childState)
////                                }
//                            )
////                                    parentContext.state = (scopedState =~ childState)(parentContext.state)
//
////                                if let scopedStateNillable {
////                                    parentContext.state = (scopedStateNillable =~ childState)(parentContext.state)
////                                }
//                        }
//                    }}
//                ])
//            },
//            update: update,
//            handleInitialAction: { _ in },
//            parentAction: { aa in
//                if let parentAction = scopedAction(aa) {
//                    handleEvent(context: parentContext, action: parentAction)
//                }
//            }
//        )
    }
}

//private func modifyIfChanged<S: Equatable, A, SS, AA>(
//    _ modify: Modification<S>,
//    _ parentContext: Context<S, A>,
//    _ context: Context<SS, AA>
//) {
//    StateQueue.asyncMain {
//        parentContext.parentStateModify(parentContext.state)
//        context.runParentActionIfExists()
//    }
////    let modified = modify(parentContext.state)
////    if modified != parentContext.state {
////        StateQueue.asyncMain {
////            parentContext.parentStateModify(parentContext.state)
////            // parentContext.state = modified
////            context.runParentActionIfExists()
////        }
//////        DispatchQueue.main.async {
//////            parentContext.state = modified
//////            context.runParentActionIfExists()
//////        }
////    } else {
////        context.runParentActionIfExists()
////    }
//}
